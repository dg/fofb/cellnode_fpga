################################################################################
# Main tcl for project
################################################################################

# ==============================================================================
proc init {} {

    addSrcModule bsp $::fwfwk::SrcPath/bsp_msk/tcl/main_mpsoc.tcl
    addSrcModule app $::fwfwk::SrcPath/app_fofbnode/tcl/main.tcl


    # Add VHDL and ADOC files when doing DesyRDL
    set ::fwfwk::addr::TypesToGen {vhdl map adoc}
    set ::fwfwk::addr::TypesToAdd {vhdl}
}

# ==============================================================================
proc setPrjProperties {} {

    fwfwk::printInfo "Setting project properties"
    set_property part ${::fwfwk::FpgaPart} [current_project]
    set_property target_language VHDL [current_project]

    # BSP config
    set fwfwk::src::bsp::Config(AXI_REG_CLK) PS
    set fwfwk::src::bsp::Config(C2C_TYPE) DIRECT
}

# ==============================================================================
proc setSources {} {

}

# ==============================================================================
proc setAddressSpace {} {

    # We want a mapp file that is align to app UIO
    addAddressSpace ::fwfwk::AddressSpace "APP" ARRAY {C9 0x00000000 8M} ::fwfwk::src::app::AddressSpace

}

# ==============================================================================
proc doOnCreate {} {

    # This project is only for vivado tool. End here for another tooltype.
    if {$::fwfwk::ToolType ne "vivado"} {
        puts "\n[ERROR] This project is only configured to work with Vivado"
        exit -1
    }


    set_property top damc_fmc2zup_top [current_fileset]
}

# ==============================================================================
proc doOnBuild {} {

    # Perform a syntax check of files
    fwfwk::printInfo "Checking syntax of files"
    set chk_syn_result [split [check_syntax -fileset sources_1 -return_string] \n]

    set syn_error 0
    foreach msg $chk_syn_result {
        if {![string length $msg]} {
            continue
        }

        if {[string match -nocase "*not compiled in library*" $msg]} {
            fwfwk::printWarning $msg
            continue
        }

        fwfwk::printError $msg
        set syn_error [expr $syn_error + 1]
    }

    if {$syn_error > 0} {
        fwfwk::printError "Syntax check failed before running simulation."
        exit -13
    }

}

# ==============================================================================
proc setSim {} {
}
