library ieee;
use ieee.std_logic_1164.all;

package pkg_app_fofb_centralnode is

  component bd_app_fofb_centralnode is
  port (
    clk_arm_250 : in STD_LOGIC;
    rstn_arm_250 : in STD_LOGIC;
    S_AXIS_IN_tdata : in STD_LOGIC_VECTOR ( 79 downto 0 );
    S_AXIS_IN_tlast : in STD_LOGIC;
    S_AXIS_IN_tready : out STD_LOGIC;
    S_AXIS_IN_tuser : in STD_LOGIC_VECTOR ( 7 downto 0 );
    S_AXIS_IN_tvalid : in STD_LOGIC;
    M_AXIS_OUT_tdata : out STD_LOGIC_VECTOR ( 47 downto 0 );
    M_AXIS_OUT_tlast : out STD_LOGIC;
    M_AXIS_OUT_tready : in STD_LOGIC;
    M_AXIS_OUT_tuser : out STD_LOGIC_VECTOR ( 7 downto 0 );
    M_AXIS_OUT_tvalid : out STD_LOGIC;
    S_AXI_FIFO_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_FIFO_arready : out STD_LOGIC;
    S_AXI_FIFO_arvalid : in STD_LOGIC;
    S_AXI_FIFO_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_FIFO_awready : out STD_LOGIC;
    S_AXI_FIFO_awvalid : in STD_LOGIC;
    S_AXI_FIFO_bready : in STD_LOGIC;
    S_AXI_FIFO_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_FIFO_bvalid : out STD_LOGIC;
    S_AXI_FIFO_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_FIFO_rready : in STD_LOGIC;
    S_AXI_FIFO_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    S_AXI_FIFO_rvalid : out STD_LOGIC;
    S_AXI_FIFO_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    S_AXI_FIFO_wready : out STD_LOGIC;
    S_AXI_FIFO_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    S_AXI_FIFO_wvalid : in STD_LOGIC
  );
  end component bd_app_fofb_centralnode;

    COMPONENT axis_downstream_fifo
    PORT (
        s_axis_aresetn : IN STD_LOGIC;
        s_axis_aclk : IN STD_LOGIC;
        s_axis_tvalid : IN STD_LOGIC;
        s_axis_tready : OUT STD_LOGIC;
        s_axis_tdata : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
        s_axis_tuser : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
        m_axis_tvalid : OUT STD_LOGIC;
        m_axis_tready : IN STD_LOGIC;
        m_axis_tdata : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
        m_axis_tuser : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
        almost_full : OUT STD_LOGIC
    );
    END COMPONENT;

end package;

--   //////////////////////////////////////////////////////////////////////////
--  //////////////////////////////////////////////////////////////////////////
-- //////////////////////////////////////////////////////////////////////////

library desy;
use desy.common_numarray.all;

package pkg_app_config is

    -----------------------
    -- DAQ CONFIGURATION --
    -----------------------
    constant C_DAQ_REGIONS              : natural := 2;
    constant C_CHANNEL_WIDTH_BYTES      : natural := 4; -- Cannot be other values. Channel is 32 bits
    constant C_DAQ_BURST_LEN_ARRAY      : t_natural_vector := (16,16,16);
    constant C_DAQ_FIFO_DEPTH_ARRAY     : t_natural_vector := (1024,1024,1024);

    -- Region 0 is continuous data
    constant C_DAQ0_IS_CONTINUOUS       : natural := 1;
    constant C_DAQ0_MAX_SAMPLES         : natural := 16#04000000#;  -- Buffer size / 8 bytes
    constant C_DAQ0_CHANNELS_IN_TAB     : natural := 2;
    constant C_DAQ0_TAB_COUNT           : natural := 1;
    constant C_DAQ0_TAB_CONTENTS        : t_natural_vector := (1, 0);
    constant C_DAQ0_BUF0_OFFSET         : natural := 16#00000000#;
    constant C_DAQ0_BUF1_OFFSET         : natural := 16#10000000#;

    -- Region 1 is pulsed data
    constant C_DAQ1_IS_CONTINUOUS       : natural := 0;
    constant C_DAQ1_MAX_SAMPLES         : natural := 16#04000000#;  -- Buffer size / 8 bytes
    constant C_DAQ1_CHANNELS_IN_TAB     : natural := 2;
    constant C_DAQ1_TAB_COUNT           : natural := 1;
    constant C_DAQ1_TAB_CONTENTS        : t_natural_vector := (1, 0);
    constant C_DAQ1_BUF0_OFFSET         : natural := 16#20000000#;
    constant C_DAQ1_BUF1_OFFSET         : natural := 16#20000000#;  -- Won't be used

    -- not used, only two regions
    constant C_DAQ2_IS_CONTINUOUS       : natural := 0;
    constant C_DAQ2_TAB_COUNT           : natural := 1;
    constant C_DAQ2_CHANNELS_IN_TAB     : natural := 1;
    constant C_DAQ2_TAB_CONTENTS        : t_natural_vector := (0=>1);
    constant C_DAQ2_BUF0_OFFSET         : natural := 0;
    constant C_DAQ2_BUF1_OFFSET         : natural := 16#80000#;

end package pkg_app_config;
