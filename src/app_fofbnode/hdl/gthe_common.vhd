library ieee;
use ieee.std_logic_1164.all;

Library UNISIM;
use UNISIM.vcomponents.all;

entity gthe_common is
    port(
        -- Differential clock intput
        gtrefclk_p      : in std_logic;
        gtrefclk_n      : in std_logic;

        -- Detection features
        freerun_clk     : in std_logic;

        -- Buffered ref
        buff_gtrefclk   : out std_logic;

        -- QPLL0 interface
        qpll0reset      : in std_logic;
        qpll0lock       : out std_logic;
        qpll0outclk     : out std_logic;
        qpll0refclk     : out std_logic;
        qpll0fblost     : out std_logic;
        qpll0reflost     : out std_logic;

        -- QPLL0 interface
        qpll1reset      : in std_logic;
        qpll1lock       : out std_logic;
        qpll1outclk     : out std_logic;
        qpll1refclk     : out std_logic;
        qpll1fblost     : out std_logic;
        qpll1reflost     : out std_logic

    );
end entity gthe_common;

architecture struct of gthe_common is

    ATTRIBUTE X_INTERFACE_INFO : STRING;
    ATTRIBUTE X_INTERFACE_INFO of gtrefclk_p: SIGNAL is "xilinx.com:interface:diff_clock:1.0 gtrefclk_diff CLK_P";
    ATTRIBUTE X_INTERFACE_INFO of gtrefclk_n: SIGNAL is "xilinx.com:interface:diff_clock:1.0 gtrefclk_diff CLK_N";

    signal gtrefclk : std_logic;
    signal gtrefclk_out2 : std_logic;


    component combpm_gtwizard_gthe4_common_wrapper
    port(
        GTHE4_COMMON_BGBYPASSB : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_BGMONITORENB : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_BGPDB : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_BGRCALOVRD : in std_logic_vector(4 downto 0);
        GTHE4_COMMON_BGRCALOVRDENB : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_DRPADDR : in std_logic_vector(15 downto 0);
        GTHE4_COMMON_DRPCLK : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_DRPDI : in std_logic_vector(15 downto 0);
        GTHE4_COMMON_DRPEN : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_DRPWE : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_GTGREFCLK0 : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_GTGREFCLK1 : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_GTNORTHREFCLK00 : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_GTNORTHREFCLK01 : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_GTNORTHREFCLK10 : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_GTNORTHREFCLK11 : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_GTREFCLK00 : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_GTREFCLK01 : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_GTREFCLK10 : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_GTREFCLK11 : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_GTSOUTHREFCLK00 : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_GTSOUTHREFCLK01 : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_GTSOUTHREFCLK10 : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_GTSOUTHREFCLK11 : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_PCIERATEQPLL0 : in std_logic_vector(2 downto 0);
        GTHE4_COMMON_PCIERATEQPLL1 : in std_logic_vector(2 downto 0);
        GTHE4_COMMON_PMARSVD0 : in std_logic_vector(7 downto 0);
        GTHE4_COMMON_PMARSVD1 : in std_logic_vector(7 downto 0);
        GTHE4_COMMON_QPLL0CLKRSVD0 : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_QPLL0CLKRSVD1 : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_QPLL0FBDIV : in std_logic_vector(7 downto 0);
        GTHE4_COMMON_QPLL0LOCKDETCLK : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_QPLL0LOCKEN : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_QPLL0PD : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_QPLL0REFCLKSEL : in std_logic_vector(2 downto 0);
        GTHE4_COMMON_QPLL0RESET : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_QPLL1CLKRSVD0 : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_QPLL1CLKRSVD1 : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_QPLL1FBDIV : in std_logic_vector(7 downto 0);
        GTHE4_COMMON_QPLL1LOCKDETCLK : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_QPLL1LOCKEN : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_QPLL1PD : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_QPLL1REFCLKSEL : in std_logic_vector(2 downto 0);
        GTHE4_COMMON_QPLL1RESET : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_QPLLRSVD1 : in std_logic_vector(7 downto 0);
        GTHE4_COMMON_QPLLRSVD2 : in std_logic_vector(4 downto 0);
        GTHE4_COMMON_QPLLRSVD3 : in std_logic_vector(4 downto 0);
        GTHE4_COMMON_QPLLRSVD4 : in std_logic_vector(7 downto 0);
        GTHE4_COMMON_RCALENB : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_SDM0DATA : in std_logic_vector(24 downto 0);
        GTHE4_COMMON_SDM0RESET : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_SDM0TOGGLE : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_SDM0WIDTH : in std_logic_vector(1 downto 0);
        GTHE4_COMMON_SDM1DATA : in std_logic_vector(24 downto 0);
        GTHE4_COMMON_SDM1RESET : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_SDM1TOGGLE : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_SDM1WIDTH : in std_logic_vector(1 downto 0);
        GTHE4_COMMON_TCONGPI : in std_logic_vector(9 downto 0);
        GTHE4_COMMON_TCONPOWERUP : in std_logic_vector(0 downto 0);
        GTHE4_COMMON_TCONRESET : in std_logic_vector(1 downto 0);
        GTHE4_COMMON_TCONRSVDIN1 : in std_logic_vector(1 downto 0);
        GTHE4_COMMON_DRPDO : out std_logic_vector(15 downto 0);
        GTHE4_COMMON_DRPRDY : out std_logic_vector(0 downto 0);
        GTHE4_COMMON_PMARSVDOUT0 : out std_logic_vector(7 downto 0);
        GTHE4_COMMON_PMARSVDOUT1 : out std_logic_vector(7 downto 0);
        GTHE4_COMMON_QPLL0FBCLKLOST : out std_logic_vector(0 downto 0);
        GTHE4_COMMON_QPLL0LOCK : out std_logic_vector(0 downto 0);
        GTHE4_COMMON_QPLL0OUTCLK : out std_logic_vector(0 downto 0);
        GTHE4_COMMON_QPLL0OUTREFCLK : out std_logic_vector(0 downto 0);
        GTHE4_COMMON_QPLL0REFCLKLOST : out std_logic_vector(0 downto 0);
        GTHE4_COMMON_QPLL1FBCLKLOST : out std_logic_vector(0 downto 0);
        GTHE4_COMMON_QPLL1LOCK : out std_logic_vector(0 downto 0);
        GTHE4_COMMON_QPLL1OUTCLK : out std_logic_vector(0 downto 0);
        GTHE4_COMMON_QPLL1OUTREFCLK : out std_logic_vector(0 downto 0);
        GTHE4_COMMON_QPLL1REFCLKLOST : out std_logic_vector(0 downto 0);
        GTHE4_COMMON_QPLLDMONITOR0 : out std_logic_vector(7 downto 0);
        GTHE4_COMMON_QPLLDMONITOR1 : out std_logic_vector(7 downto 0);
        GTHE4_COMMON_REFCLKOUTMONITOR0 : out std_logic_vector(0 downto 0);
        GTHE4_COMMON_REFCLKOUTMONITOR1 : out std_logic_vector(0 downto 0);
        GTHE4_COMMON_RXRECCLK0SEL : out std_logic_vector(1 downto 0);
        GTHE4_COMMON_RXRECCLK1SEL : out std_logic_vector(1 downto 0);
        GTHE4_COMMON_SDM0FINALOUT : out std_logic_vector(3 downto 0);
        GTHE4_COMMON_SDM0TESTDATA : out std_logic_vector(14 downto 0);
        GTHE4_COMMON_SDM1FINALOUT : out std_logic_vector(3 downto 0);
        GTHE4_COMMON_SDM1TESTDATA : out std_logic_vector(14 downto 0);
        GTHE4_COMMON_TCONGPO : out std_logic_vector(9 downto 0);
        GTHE4_COMMON_TCONRSVDOUT0 : out std_logic_vector(0 downto 0)
    );
    end component;

begin

    -- INPUT BUFFER
   IBUFDS_GTE4_inst : IBUFDS_GTE4
   generic map (
      REFCLK_EN_TX_PATH => '0',
      REFCLK_HROW_CK_SEL => "00",
      REFCLK_ICNTL_RX => "00"
   )
   port map (
      O => gtrefclk,
      ODIV2 => gtrefclk_out2,
      CEB => '0',
      I => gtrefclk_p,
      IB => gtrefclk_n
   );

   BUFG_GT_inst : BUFG_GT
   port map (
      O => buff_gtrefclk,
      CE => '1',
      CEMASK => '1',
      CLR => '0',
      CLRMASK => '1',
      DIV => "000",
      I => gtrefclk_out2
   );

    -- TRANSCEIVER COMMON BLOCK
    gthe4_common_wrapper_inst: combpm_gtwizard_gthe4_common_wrapper
    port map(
        GTHE4_COMMON_BGBYPASSB =>         "1",
        GTHE4_COMMON_BGMONITORENB =>      "1",
        GTHE4_COMMON_BGPDB =>             "1",
        GTHE4_COMMON_BGRCALOVRD =>        "11111",
        GTHE4_COMMON_BGRCALOVRDENB =>     "1",
        GTHE4_COMMON_DRPADDR =>           "0000000000000000",
        GTHE4_COMMON_DRPCLK =>            "0",
        GTHE4_COMMON_DRPDI =>             "0000000000000000",
        GTHE4_COMMON_DRPEN =>             "0",
        GTHE4_COMMON_DRPWE =>             "0",
        GTHE4_COMMON_GTGREFCLK0 =>        "0",
        GTHE4_COMMON_GTGREFCLK1 =>        "0",
        GTHE4_COMMON_GTNORTHREFCLK00 =>   "0",
        GTHE4_COMMON_GTNORTHREFCLK01 =>   "0",
        GTHE4_COMMON_GTNORTHREFCLK10 =>   "0",
        GTHE4_COMMON_GTNORTHREFCLK11 =>   "0",
        GTHE4_COMMON_GTREFCLK00(0) =>        gtrefclk,
        GTHE4_COMMON_GTREFCLK01(0) =>        gtrefclk,
        GTHE4_COMMON_GTREFCLK10 =>        "0",
        GTHE4_COMMON_GTREFCLK11 =>        "0",
        GTHE4_COMMON_GTSOUTHREFCLK00 =>   "0",
        GTHE4_COMMON_GTSOUTHREFCLK01 =>   "0",
        GTHE4_COMMON_GTSOUTHREFCLK10 =>   "0",
        GTHE4_COMMON_GTSOUTHREFCLK11 =>   "0",
        GTHE4_COMMON_PCIERATEQPLL0 =>     "000",
        GTHE4_COMMON_PCIERATEQPLL1 =>     "000",
        GTHE4_COMMON_PMARSVD0 =>          "00000000",
        GTHE4_COMMON_PMARSVD1 =>          "00000000",
        GTHE4_COMMON_QPLL0CLKRSVD0 =>     "0",
        GTHE4_COMMON_QPLL0CLKRSVD1 =>     "0",
        GTHE4_COMMON_QPLL0FBDIV =>        "00000000",
        GTHE4_COMMON_QPLL0LOCKDETCLK(0) =>freerun_clk,
        GTHE4_COMMON_QPLL0LOCKEN =>       "1",
        GTHE4_COMMON_QPLL0PD =>           "0",
        GTHE4_COMMON_QPLL0REFCLKSEL =>    "001",
        GTHE4_COMMON_QPLL0RESET(0) =>        qpll0reset,
        GTHE4_COMMON_QPLL1CLKRSVD0 =>     "0",
        GTHE4_COMMON_QPLL1CLKRSVD1 =>     "0",
        GTHE4_COMMON_QPLL1FBDIV =>        "00000000",
        GTHE4_COMMON_QPLL1LOCKDETCLK(0) =>freerun_clk,
        GTHE4_COMMON_QPLL1LOCKEN =>       "1",
        GTHE4_COMMON_QPLL1PD =>           "0",
        GTHE4_COMMON_QPLL1REFCLKSEL =>    "001",
        GTHE4_COMMON_QPLL1RESET(0) =>        qpll1reset,
        GTHE4_COMMON_QPLLRSVD1 =>         "00000000",
        GTHE4_COMMON_QPLLRSVD2 =>         "00000",
        GTHE4_COMMON_QPLLRSVD3 =>         "00000",
        GTHE4_COMMON_QPLLRSVD4 =>         "00000000",
        GTHE4_COMMON_RCALENB =>           "1",
        GTHE4_COMMON_SDM0DATA =>          "0000000000000000000000000",
        GTHE4_COMMON_SDM0RESET =>         "0",
        GTHE4_COMMON_SDM0TOGGLE =>        "0",
        GTHE4_COMMON_SDM0WIDTH =>         "00",
        GTHE4_COMMON_SDM1DATA =>          "0010001011010000111001010",
        GTHE4_COMMON_SDM1RESET =>         "0",
        GTHE4_COMMON_SDM1TOGGLE =>        "0",
        GTHE4_COMMON_SDM1WIDTH =>         "00",
        GTHE4_COMMON_TCONGPI =>           "0000000000",
        GTHE4_COMMON_TCONPOWERUP =>       "0",
        GTHE4_COMMON_TCONRESET =>         "00",
        GTHE4_COMMON_TCONRSVDIN1 =>       "00",
        GTHE4_COMMON_DRPDO =>             open,
        GTHE4_COMMON_DRPRDY =>            open,
        GTHE4_COMMON_PMARSVDOUT0 =>       open,
        GTHE4_COMMON_PMARSVDOUT1 =>       open,
        GTHE4_COMMON_QPLL0FBCLKLOST(0)  =>      qpll0fblost,
        GTHE4_COMMON_QPLL0LOCK(0) =>         qpll0lock,
        GTHE4_COMMON_QPLL0OUTCLK(0) =>       qpll0outclk,
        GTHE4_COMMON_QPLL0OUTREFCLK(0) =>    qpll0refclk,
        GTHE4_COMMON_QPLL0REFCLKLOST(0)  =>   qpll0reflost,
        GTHE4_COMMON_QPLL1FBCLKLOST(0)  =>    qpll1reflost,
        GTHE4_COMMON_QPLL1LOCK(0) =>         qpll1lock,
        GTHE4_COMMON_QPLL1OUTCLK(0) =>       qpll1outclk,
        GTHE4_COMMON_QPLL1OUTREFCLK(0) =>    qpll1refclk,
        GTHE4_COMMON_QPLL1REFCLKLOST(0)  =>   qpll1fblost,
        GTHE4_COMMON_QPLLDMONITOR0 =>     open,
        GTHE4_COMMON_QPLLDMONITOR1 =>     open,
        GTHE4_COMMON_REFCLKOUTMONITOR0 => open,
        GTHE4_COMMON_REFCLKOUTMONITOR1 => open,
        GTHE4_COMMON_RXRECCLK0SEL =>      open,
        GTHE4_COMMON_RXRECCLK1SEL =>      open,
        GTHE4_COMMON_SDM0FINALOUT =>      open,
        GTHE4_COMMON_SDM0TESTDATA =>      open,
        GTHE4_COMMON_SDM1FINALOUT =>      open,
        GTHE4_COMMON_SDM1TESTDATA =>      open,
        GTHE4_COMMON_TCONGPO =>           open,
        GTHE4_COMMON_TCONRSVDOUT0 =>      open
    );

end architecture struct;
