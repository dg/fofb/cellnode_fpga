-- Wrapper for CACTUS RTM
-- The RTM is connected throught the Spartan7 on the DAMC-FMC2ZUP
-- ZUPER LINK is not used. Spartan7 is configured to pass-through

library ieee;
use ieee.std_logic_1164.all;


entity rtm_cactus_top is
    port(
        -- == == RTM SIDE, SPARTAN7 == == --
        c2c_data_p   : out std_logic_vector (20 downto 1);
        c2c_data_n   : out std_logic_vector (20 downto 1);

        -- == == LOGIC SIDE == == --
        rtm_tx       : in std_logic_vector(31 downto 0);
        rtm_tx_en    : in std_logic_vector(7 downto 0)
    );
end entity;

architecture struct of rtm_cactus_top is

begin

    ----------------
    -- RTM CACTUS --
    ----------------
    c2c_data_p(1)  <= rtm_tx(30);
    c2c_data_n(1)  <= rtm_tx(31);
    c2c_data_p(2)  <= rtm_tx(28);
    c2c_data_n(2)  <= rtm_tx(29);
    c2c_data_p(3)  <= rtm_tx_en(7);
    c2c_data_n(3)  <= rtm_tx_en(6);
    c2c_data_p(4)  <= rtm_tx(26);
    c2c_data_n(4)  <= rtm_tx(27);
    c2c_data_p(5)  <= rtm_tx(24);
    c2c_data_n(5)  <= rtm_tx(25);
    c2c_data_p(6)  <= rtm_tx(22);
    c2c_data_n(6)  <= rtm_tx(23);
    c2c_data_p(7)  <= rtm_tx(20);
    c2c_data_n(7)  <= rtm_tx(21);
    c2c_data_p(8)  <= rtm_tx_en(5);
    c2c_data_n(8)  <= rtm_tx_en(4);
    c2c_data_p(9)  <= rtm_tx(18);
    c2c_data_n(9)  <= rtm_tx(19);
    c2c_data_p(10) <= rtm_tx(16);
    c2c_data_n(10) <= rtm_tx(17);
    c2c_data_p(11) <= rtm_tx_en(3);
    c2c_data_n(11) <= rtm_tx(14);
    c2c_data_p(12) <= rtm_tx(15);
    c2c_data_n(12) <= rtm_tx(12);
    c2c_data_p(13) <= rtm_tx(13);
    c2c_data_n(13) <= rtm_tx_en(2);
    c2c_data_p(14) <= rtm_tx(10);
    c2c_data_n(14) <= rtm_tx(11);
    c2c_data_p(15) <= rtm_tx(8);
    c2c_data_n(15) <= rtm_tx(9);
    c2c_data_p(16) <= rtm_tx_en(1);
    c2c_data_n(16) <= rtm_tx(6);
    c2c_data_p(17) <= rtm_tx(7);
    c2c_data_n(17) <= rtm_tx(4);
    c2c_data_p(18) <= rtm_tx(5);
    c2c_data_n(18) <= rtm_tx_en(0);
    c2c_data_p(19) <= rtm_tx(2);
    c2c_data_n(19) <= rtm_tx(3);
    c2c_data_p(20) <= rtm_tx(0);
    c2c_data_n(20) <= rtm_tx(1);

end architecture;
