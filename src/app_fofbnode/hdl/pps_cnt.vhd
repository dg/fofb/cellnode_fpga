library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.log2;
use ieee.math_real.ceil;

entity pps_cnt is
    generic(
        G_PULSE_W   : natural := 4;
        G_CLK_FREQ  : natural;
        G_W_TS      : natural := 64
    );
    port(
        rst_n       : in std_logic;
        clk         : in std_logic;
        srst        : in std_logic;
        pps         : out std_logic;
        timestamp   : out std_logic_vector(G_W_TS-1 downto 0)
    );
end entity pps_cnt;

architecture rtl of pps_cnt is

    -- Constants and type for timeref subcounter
    constant C_W_SUBCNT         : natural                      := 16;
    constant C_N_SUBCNT         : natural                      := natural(ceil(real(G_W_TS)/real(C_W_SUBCNT)));

    type t_subcnt is array(0 to C_N_SUBCNT-1) of unsigned(C_W_SUBCNT-1 downto 0);
    type t_2d_subcnt is array(0 to C_N_SUBCNT-1) of t_subcnt;

    -- Constant for PPS counter
    constant C_W_CNT            : natural                      := natural(ceil(log2(real(G_CLK_FREQ))));
    constant C_ROLL             : unsigned(C_W_CNT-1 downto 0) := to_unsigned(G_CLK_FREQ-1, C_W_CNT);

    signal flat_refcounter      : unsigned(C_W_SUBCNT*C_N_SUBCNT-1 downto 0);

    signal refcounter           : t_subcnt;
    signal r_refcounter         : t_2d_subcnt;
    signal r_msb                : std_logic_vector(C_N_SUBCNT-1 downto 0);

    signal counter              : unsigned(C_W_CNT-1 downto 0);
    signal s_pps                : std_logic_vector(G_PULSE_W-1 downto 0);

begin

    pps         <= s_pps(0);
    timestamp   <= std_logic_vector(flat_refcounter(G_W_TS-1 downto 0));

    p_pps:process(clk, rst_n)
    begin
        if rst_n = '0' then
            counter <= C_ROLL;
            s_pps   <= (others => '0');
        elsif rising_edge(clk) then
            if counter = 0 then
                counter <= C_ROLL;
                s_pps   <= (others => '1');
            else
                counter <= counter-1;
                s_pps   <= '0' & s_pps(s_pps'left downto 1);
            end if;
        end if;
    end process;

    p_timestamp:process(clk, rst_n)
    begin
        if rst_n = '0' then
            refcounter  <= (others => (others => '0'));
            r_msb       <= (others => '0');
        elsif rising_edge(clk) then
            if srst = '1' then
                refcounter  <= (others => (others => '0'));
                r_msb       <= (others => '0');
            else

                -- Reference increment
                refcounter(0) <= refcounter(0)+1;

                -- Register all msb
                for I in 0 to C_N_SUBCNT-1 loop
                    r_msb(I)    <= refcounter(I)(C_W_SUBCNT-1);
                end loop;

                -- Propagate carry
                for I in 0 to C_N_SUBCNT-2 loop
                    if r_msb(I) = '1' and refcounter(I)(C_W_SUBCNT-1) = '0' then
                        refcounter(I+1) <= refcounter(I+1)+1;
                    end if;
                end loop;

                -- Registers for pipline. Most will be deleted because unused (triangular).
                for I in 0 to C_N_SUBCNT-1 loop
                    r_refcounter(I)(0) <= refcounter(I);
                    for J in 0 to C_N_SUBCNT-2 loop
                        r_refcounter(I)(J+1) <= r_refcounter(I)(J);
                    end loop;
                end loop;

            end if;
        end if;
    end process p_timestamp;

    -- Connect
    g_timeref:for I in 0 to C_N_SUBCNT-2 generate
        flat_refcounter((I+1)*C_W_SUBCNT-1 downto I*C_W_SUBCNT) <= r_refcounter(I)(C_N_SUBCNT-1-I);
    end generate;


end architecture rtl;


