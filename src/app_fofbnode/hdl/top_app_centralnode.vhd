library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.and_reduce;

library unisim;
use unisim.vcomponents.all;

use work.pkg_bsp_fmc2zup_payload.all;
use work.pkg_app_fofb_centralnode.all;
use work.pkg_app_config.all;

library desy;
use desy.common_to_desyrdl.all;
use desy.common_types.t_32b_slv_vector;
use desy.common_bsp_ifs.all;
use desy.common_axi.t_axi4_m2s;
use desy.common_axi.t_axi4_s2m;

library ccn_eth_lib;


-- top interface and address space
library desyrdl;
use desyrdl.common.all;
use desyrdl.pkg_app_fofb_centralnode.all;
use desyrdl.pkg_ccn_packeter.t_ccn_packeter_m2s;
use desyrdl.pkg_ccn_packeter.t_ccn_packeter_s2m;
use desyrdl.pkg_ccn_unpacketer.t_ccn_unpacketer_m2s;
use desyrdl.pkg_ccn_unpacketer.t_ccn_unpacketer_s2m;
use desyrdl.pkg_ccn_ethernet.t_ccn_ethernet_s2m;
use desyrdl.pkg_ccn_ethernet.t_ccn_ethernet_m2s;

entity bsp_fmc2zup_payload is
    port (
        pi_payload  : in    t_payload_i;
        po_payload  : out   t_payload_o;
        pio_payload : inout t_payload_io
    );
end entity bsp_fmc2zup_payload;

architecture struct of bsp_fmc2zup_payload is

    constant C_LOGIC_ZERO : std_logic := '0';
    constant C_TID0       : std_logic_vector(1 downto 0) := "00";
    constant C_TID1       : std_logic_vector(1 downto 0) := "01";
    constant C_TID2       : std_logic_vector(1 downto 0) := "10";
    constant C_TID3       : std_logic_vector(1 downto 0) := "11";

    ----------------------
    -- TYPE DECLARATION --
    ----------------------
    -- AXI-MM distributed
    type t_a_ccneth_m2s is array (0 to 3) of t_ccn_ethernet_m2s;
    type t_a_ccneth_s2m is array (0 to 3) of t_ccn_ethernet_s2m;
    type t_a_ccnpkt_m2s is array (0 to 3) of t_ccn_packeter_m2s;
    type t_a_ccnpkt_s2m is array (0 to 3) of t_ccn_packeter_s2m;
    type t_a_ccnupk_m2s is array (0 to 3) of t_ccn_unpacketer_m2s;
    type t_a_ccnupk_s2m is array (0 to 3) of t_ccn_unpacketer_s2m;

    -- AXIS distributed
    type t_a_axis_pkt_rx_tuser is array(0 to 3) of std_logic_vector(9 downto 0);
    type t_a_axis_pkt_rx_tdata is array(0 to 3) of std_logic_vector(79 downto 0);

    ------------------------
    -- SIGNAL DECLARATION --
    ------------------------

    -- clocks and resets
    signal axis_clk             : std_logic;
    signal axis_rstn            : std_logic;
    signal axis_rst             : std_logic;

    -- Distributed clock for every branch
    signal clk_ccn_a    : std_logic_vector(3 downto 0);
    signal clk_ccn      : std_logic;

    -- AXI-MM Arbiter
    signal addrmap_i            : t_addrmap_app_fofb_centralnode_in;
    signal addrmap_o            : t_addrmap_app_fofb_centralnode_out;
    signal s_reg_if_m2s         : t_app_fofb_centralnode_m2s;
    signal s_reg_if_s2m         : t_app_fofb_centralnode_s2m;

    -- AXI-MM distributed
    signal ccn_eth_am2s : t_a_ccneth_m2s;
    signal ccn_eth_as2m : t_a_ccneth_s2m;
    signal ccn_pkt_am2s : t_a_ccnpkt_m2s;
    signal ccn_pkt_as2m : t_a_ccnpkt_s2m;
    signal ccn_upk_am2s : t_a_ccnupk_m2s;
    signal ccn_upk_as2m : t_a_ccnupk_s2m;

    -- FMC2 (4SFP+) signals
    signal fmc2_sfp_tx_n        : std_logic_vector(3 downto 0);
    signal fmc2_sfp_tx_p        : std_logic_vector(3 downto 0);
    signal fmc2_sfp_rx_n        : std_logic_vector(3 downto 0);
    signal fmc2_sfp_rx_p        : std_logic_vector(3 downto 0);
    signal fmc2_sfp_rxlos       : std_logic_vector(3 downto 0);
    signal fmc2_sfp_mod_abs     : std_logic_vector(3 downto 0);
    signal fmc2_sfp_tx_disable  : std_logic_vector(3 downto 0);
    signal fmc2_sfp_tx_fault    : std_logic_vector(3 downto 0);
    signal fmc2_ref_clk_n       : std_logic;
    signal fmc2_ref_clk_p       : std_logic;

    -- QPLL common quad signals
    signal qpll0reset           : std_logic;
    signal qpll0lock            : std_logic;
    signal qpll0outclk          : std_logic;
    signal qpll0refclk          : std_logic;
    signal qpll0fblost          : std_logic;
    signal qpll0reflost         : std_logic;

    -- COMCELLNODE signals

    signal qpll_reset           : std_logic_vector(3 downto 0);
    signal gt_powergood         : std_logic_vector(3 downto 0);

    -- AXIS unpacketer
    signal axis_pkt_rx_tvalid_a      : std_logic_vector(0 to 3);
    signal axis_pkt_rx_tready_a      : std_logic_vector(0 to 3);
    signal axis_pkt_rx_tlast_a       : std_logic_vector(0 to 3);
    signal axis_pkt_rx_tdata_a       : t_a_axis_pkt_rx_tdata;
    signal axis_pkt_rx_tuser_a       : t_a_axis_pkt_rx_tuser;

    -- AXIS grouped unpacketer
    signal axis_pkt_rx_tvalid   : std_logic;
    signal axis_pkt_rx_tready   : std_logic;
    signal axis_pkt_rx_tlast    : std_logic;
    signal axis_pkt_rx_tid      : std_logic_vector(1 downto 0);
    signal axis_pkt_rx_tuser    : std_logic_vector(7 downto 0);
    signal axis_pkt_rx_tdata    : std_logic_vector(79 downto 0);

    -- AXIS correction
    signal axis_corr_tvalid     : std_logic;
    signal axis_corr_tuser      : std_logic_vector(7 downto 0);
    signal axis_corr_tdata      : std_logic_vector(23 downto 0);


    -- PSCGEN output
    signal axis_pscgen_tdata_pscid  : std_logic_vector(7 downto 0);
    signal axis_pscgen_tdata_value  : std_logic_vector(15 downto 0);
    signal axis_pscgen_tuser        : std_logic_vector(7 downto 0);
    signal axis_pscgen_tvalid       : std_logic;

    signal axis_corrfifo_tvalid     : std_logic;
    signal axis_corrfifo_tready     : std_logic;
    signal axis_corrfifo_tuser      : std_logic_vector(7 downto 0);
    signal axis_corrfifo_tdata      : std_logic_vector(47 downto 0);
    signal axis_corrfifo_tdata_sm   : std_logic_vector(23 downto 0);


    -- AXIS grouped packeter
    signal axis_pkt_tx_tvalid     : std_logic_vector(3 downto 0);
    signal axis_pkt_tx_tready     : std_logic_vector(3 downto 0);
    signal axis_pkt_tx_tuser      : std_logic_vector(4*8-1 downto 0);
    signal axis_pkt_tx_tdata      : std_logic_vector(4*48-1 downto 0);

    -- SYNCHRO signals
    signal pps                  : std_logic;
    signal mc_time              : std_logic_vector(39 downto 0);
    signal timeref              : std_logic_vector(63 downto 0);

    -- DAQ
    signal m_axi4_daq_s2m       : t_axi4_s2m;
    signal m_axi4_daq_m2s       : t_axi4_m2s;
    signal daq_strobe           : std_logic_vector(1 downto 0);
    signal daq_trigger          : std_logic;
    signal daq_stop             : std_logic;
    signal r_daq_trigger        : std_logic_vector(1 downto 0);
    signal daq_data             : t_32b_slv_vector(3 downto 0);
    signal daq_irq              : std_logic_vector(C_DAQ_REGIONS-1 downto 0);
    signal r_last_seq           : std_logic_vector(7 downto 0);

    signal axis_daqs00_tdata    : std_logic_vector(63 downto 0);
    signal axis_daqs00_tvalid   : std_logic;
    signal axis_daqs01_tdata    : std_logic_vector(63 downto 0);
    signal axis_daqs01_tvalid   : std_logic;
    signal axis_daqs02_tdata    : std_logic_vector(63 downto 0);
    signal axis_daqs02_tvalid   : std_logic;
    signal axis_daqs03_tdata    : std_logic_vector(63 downto 0);
    signal axis_daqs03_tvalid   : std_logic;

    -- Delayed bpm DAQ, in order to have the new seq timestamp first in the capture
    signal d1_axis_daqs01_tdata    : std_logic_vector(63 downto 0);
    signal d1_axis_daqs01_tvalid   : std_logic;
    signal d1_axis_daqs02_tdata    : std_logic_vector(63 downto 0);
    signal d1_axis_daqs02_tvalid   : std_logic;
    signal d2_axis_daqs01_tdata    : std_logic_vector(63 downto 0);
    signal d2_axis_daqs01_tvalid   : std_logic;
    signal d2_axis_daqs02_tdata    : std_logic_vector(63 downto 0);
    signal d2_axis_daqs02_tvalid   : std_logic;

    signal axis_daqm00_tdata    : std_logic_vector(63 downto 0);
    signal axis_daqm00_tvalid   : std_logic;
    signal axis_daqm00_tready   : std_logic;

    -- Other signals
    signal rst                  : std_logic;




begin

    -- Stream circuitry is on axi4l clock domain
    axis_clk    <= pi_payload.m_axi4l_reg_aclk;
    axis_rstn   <= pi_payload.m_axi4l_reg_areset_n;
    axis_rst    <= not axis_rstn;

    rst     <= not pi_payload.m_axi4l_reg_areset_n;

    ------------------------------
    -- Assign not used BSP signals
    ------------------------------
    -- MLVDS
    po_payload.mlvds    <= x"00";
    po_payload.mlvds_oe <= x"00";

    -- place buffer and assign IO standard
    inst_fmc1_sync_buf   : OBUFDS
    generic map (IOSTANDARD => "LVDS")
    port map( O => po_payload.fmc1.sync_p, OB =>  po_payload.fmc1.sync_n , I => '0');

    inst_fmc1_refclk_buf : OBUFDS
    generic map (IOSTANDARD => "LVDS")
    port map( O => po_payload.fmc1.refclk_p, OB =>  po_payload.fmc1.refclk_n , I => '0');


    --------------------
    -- AXI-MM ARBITER --
    --------------------
    s_reg_if_m2s            <= f_common_to_desyrdl(pi_payload.m_axi4l_reg);
    po_payload.m_axi4l_reg  <= f_common_to_desyrdl(s_reg_if_s2m);

    ins_app_desyrdl : entity desyrdl.app_fofb_centralnode
    port map (
        pi_clock     => pi_payload.m_axi4l_reg_aclk,
        pi_reset     => rst,
        pi_s_top     => s_reg_if_m2s,
        po_s_top     => s_reg_if_s2m,
        pi_addrmap   => addrmap_i,
        po_addrmap   => addrmap_o
    );


    -----------------------
    -- FMC 4SFP+ CAENELS --
    -----------------------
    inst_fmc2_4sfp: entity work.fmcbsp_4sfp
    port map(
        -- == == FMC SIDE == == --
        -- Transceivers clocks
        fmc_gbtclk0_p                              => pi_payload.fmc2_mgt.clk_p(0),
        fmc_gbtclk0_n                              => pi_payload.fmc2_mgt.clk_n(0),

        -- LA pins, connect all but uses a few
        fmc_la_p                                   => pio_payload.fmc2.la_p,
        fmc_la_n                                   => pio_payload.fmc2.la_n,

        -- MGT differential pairs, only fours used
        fmc_dp_m2c_p                               => pi_payload.fmc2_mgt.dp_p(3 downto 0),
        fmc_dp_m2c_n                               => pi_payload.fmc2_mgt.dp_n(3 downto 0),
        fmc_dp_c2m_p                               => po_payload.fmc2_mgt.dp_p(3 downto 0),
        fmc_dp_c2m_n                               => po_payload.fmc2_mgt.dp_n(3 downto 0),

        -- == == LOGIC SIDE == == --
        osc_i2c_scl                                => '0',
        osc_i2c_sda                                => open,
        osc_ref_clk_p                              => fmc2_ref_clk_p,
        osc_ref_clk_n                              => fmc2_ref_clk_n,
        sfp_tx_n                                   => fmc2_sfp_tx_n,
        sfp_tx_p                                   => fmc2_sfp_tx_p,
        sfp_rx_n                                   => fmc2_sfp_rx_n,
        sfp_rx_p                                   => fmc2_sfp_rx_p,
        sfp_rxlos                                  => fmc2_sfp_rxlos,
        sfp_mod_abs                                => fmc2_sfp_mod_abs,
        sfp_tx_disable                             => fmc2_sfp_tx_disable,
        sfp_tx_fault                               => fmc2_sfp_tx_fault,
        sfp_rs0                                    => (others => '0'),
        sfp_rs1                                    => (others => '0'),
        sfp_i2c_scl                                => (others => '0'),
        sfp_i2c_sda                                => open
    );

    ------------------------
    -- GTHE COMMON (QPLL) --
    ------------------------
    inst_gthe_common: entity work.gthe_common
    port map(
        -- Differential clock intput
        gtrefclk_p      => fmc2_ref_clk_p,
        gtrefclk_n      => fmc2_ref_clk_n,

        -- Detection features (100 MHz clk)
        freerun_clk     => pi_payload.ps_clk0,

        -- Buffered ref
        buff_gtrefclk   => open,

        -- QPLL0 interface (10GbE)
        qpll0reset      => qpll0reset,
        qpll0lock       => qpll0lock,
        qpll0outclk     => qpll0outclk,
        qpll0refclk     => qpll0refclk,
        qpll0fblost     => qpll0fblost,
        qpll0reflost    => qpll0reflost,

        -- QPLL1 interface (COMBPM)
        qpll1reset      => qpll0reset,  -- This QPLL is not used. we reset with the same signal
        qpll1lock       => open,
        qpll1outclk     => open,
        qpll1refclk     => open,
        qpll1fblost     => open,
        qpll1reflost    => open
    );
    qpll0reset <= not and_reduce(gt_powergood);

    -----------------
    -- COMCELLNODE --
    -----------------
    -- link to axi-mm
    ccn_eth_am2s(0) <= addrmap_o.ccn_ethernet_0;
    addrmap_i.ccn_ethernet_0 <= ccn_eth_as2m(0);
    ccn_eth_am2s(1) <= addrmap_o.ccn_ethernet_1;
    addrmap_i.ccn_ethernet_1 <= ccn_eth_as2m(1);
    ccn_eth_am2s(2) <= addrmap_o.ccn_ethernet_2;
    addrmap_i.ccn_ethernet_2 <= ccn_eth_as2m(2);
    ccn_eth_am2s(3) <= addrmap_o.ccn_ethernet_3;
    addrmap_i.ccn_ethernet_3 <= ccn_eth_as2m(3);

    ccn_pkt_am2s(0) <= addrmap_o.ccn_packeter_0;
    addrmap_i.ccn_packeter_0 <= ccn_pkt_as2m(0);
    ccn_pkt_am2s(1) <= addrmap_o.ccn_packeter_1;
    addrmap_i.ccn_packeter_1 <= ccn_pkt_as2m(1);
    ccn_pkt_am2s(2) <= addrmap_o.ccn_packeter_2;
    addrmap_i.ccn_packeter_2 <= ccn_pkt_as2m(2);
    ccn_pkt_am2s(3) <= addrmap_o.ccn_packeter_3;
    addrmap_i.ccn_packeter_3 <= ccn_pkt_as2m(3);

    ccn_upk_am2s(0) <= addrmap_o.ccn_unpacketer_0;
    addrmap_i.ccn_unpacketer_0 <= ccn_upk_as2m(0);
    ccn_upk_am2s(1) <= addrmap_o.ccn_unpacketer_1;
    addrmap_i.ccn_unpacketer_1 <= ccn_upk_as2m(1);
    ccn_upk_am2s(2) <= addrmap_o.ccn_unpacketer_2;
    addrmap_i.ccn_unpacketer_2 <= ccn_upk_as2m(2);
    ccn_upk_am2s(3) <= addrmap_o.ccn_unpacketer_3;
    addrmap_i.ccn_unpacketer_3 <= ccn_upk_as2m(3);

    clk_ccn <= clk_ccn_a(0);

    gen_ccn:for I in 0 to 3 generate

        signal axis_tx_tvalid       : std_logic;
        signal axis_tx_tready       : std_logic;
        signal axis_tx_tdata        : std_logic_vector(63 downto 0);
        signal axis_tx_tkeep        : std_logic_vector(7 downto 0);
        signal axis_tx_tlast        : std_logic;
        signal axis_tx_tuser        : std_logic_vector(0 downto 0);
        signal axis_rx_tvalid       : std_logic;
        signal axis_rx_tready       : std_logic;
        signal axis_rx_tuser        : std_logic_vector(0 downto 0);
        signal axis_rx_tlast        : std_logic;
        signal axis_rx_tkeep        : std_logic_vector(7 downto 0);
        signal axis_rx_tdata        : std_logic_vector(63 downto 0);

    begin

        ------------------
        -- CCN ETHERNET --
        ------------------
        inst_ccn_ethernet: entity ccn_eth_lib.top_ccn_ethernet
        generic map(G_IPNUM => I)
        port map(
            aresetn                     => pi_payload.m_axi4l_reg_areset_n,
            free_100_clk                => pi_payload.ps_clk1,
            aclk                        => pi_payload.m_axi4l_reg_aclk,
            axis_clk                    => clk_ccn_a(I),

            -- Common Quad signals
            qpll0clk_in                 => qpll0outclk,
            qpll0refclk_in              => qpll0refclk,
            qpll0lock_in                => qpll0lock,
            qpll0reset_out              => qpll_reset(I),
            gt_powergood                => gt_powergood(I),

            -- AXI-MM Status and Config
            s_axi_m2s                   => ccn_eth_am2s(I),
            s_axi_s2m                   => ccn_eth_as2m(I),

            pps                         => pps,

            -- AXIS TX RX
            s_axis_tx_tvalid            => axis_tx_tvalid,
            s_axis_tx_tready            => axis_tx_tready,
            s_axis_tx_tdata             => axis_tx_tdata,
            s_axis_tx_tkeep             => axis_tx_tkeep,
            s_axis_tx_tlast             => axis_tx_tlast,
            s_axis_tx_tuser             => axis_tx_tuser(0),
            m_axis_rx_tvalid            => axis_rx_tvalid,
            m_axis_rx_tuser             => axis_rx_tuser(0),
            m_axis_rx_tlast             => axis_rx_tlast,
            m_axis_rx_tkeep             => axis_rx_tkeep,
            m_axis_rx_tdata             => axis_rx_tdata,

            -- SFP signals
            sfp_txp                     => fmc2_sfp_tx_p(I),
            sfp_txn                     => fmc2_sfp_tx_n(I),
            sfp_rxp                     => fmc2_sfp_rx_p(I),
            sfp_rxn                     => fmc2_sfp_rx_n(I),
            sfp_rx_los                  => fmc2_sfp_rxlos(I),
            sfp_mod_abs                 => fmc2_sfp_mod_abs(I),
            sfp_tx_disable              => fmc2_sfp_tx_disable(I),
            sfp_tx_fault                => fmc2_sfp_tx_fault(I)
        );

        --------------------
        -- CCN UNPACKETER --
        --------------------
        inst_ccn_unpacketer: entity work.top_ccn_unpacketer
        generic map(
            G_S_TDATA_W          => 64,
            G_S_TUSER_W          => 1,
            G_M_TDATA_W          => 80,
            G_M_TUSER_W          => 8,
            G_PROTOCOL_ID        => 172
        )
        port map(
            aresetn                     => pi_payload.m_axi4l_reg_areset_n,

            -- Running timeref
            timeref                     => timeref,

            -- AXI-MM Status and Config
            aclk                        => axis_clk,
            s_axi_m2s                   => ccn_upk_am2s(I),
            s_axi_s2m                   => ccn_upk_as2m(I),

            -- AXIS Frame input
            s_axis_clk                  => clk_ccn_a(I),
            s_axis_tdata                => axis_rx_tdata,
            s_axis_tkeep                => axis_rx_tkeep,
            s_axis_tlast                => axis_rx_tlast,
            s_axis_tvalid               => axis_rx_tvalid,
            s_axis_tready               => axis_rx_tready,
            s_axis_tuser                => axis_rx_tuser,

            -- AXIS Packet output
            m_axis_tdata                => axis_pkt_rx_tdata_a(I),
            m_axis_tuser                => axis_pkt_rx_tuser_a(I)(7 downto 0),
            m_axis_tvalid               => axis_pkt_rx_tvalid_a(I),
            m_axis_tready               => axis_pkt_rx_tready_a(I),
            m_axis_tlast                => axis_pkt_rx_tlast_a(I)

        );

        axis_pkt_rx_tuser_a(I)(9 downto 8) <= (others => '0');

        -- TODO: unpacketer detect frame input overrun (tready='1' and tvalid='1')


        ------------------
        -- CCN PACKETER --
        ------------------
        inst_ccn_packeter: entity work.top_ccn_packeter
        generic map(
            G_S_TDATA_W          => 48,
            G_S_TUSER_W          => 8,
            G_M_TDATA_W          => 64,
            G_M_TUSER_W          => 1,
            G_PROTOCOL_ID        => 182
        )
        port map(
            aresetn                     => pi_payload.m_axi4l_reg_areset_n,

            -- Running timeref
            timeref                     => timeref,

            -- AXI-MM Status and Config
            aclk                        => pi_payload.m_axi4l_reg_aclk,
            s_axi_m2s                   => ccn_pkt_am2s(I),
            s_axi_s2m                   => ccn_pkt_as2m(I),

            -- AXIS Packet input
            s_axis_tdata                => axis_pkt_tx_tdata((I+1)*48-1 downto I*48),
            s_axis_tuser                => axis_pkt_tx_tuser((I+1)*8-1 downto I*8),
            s_axis_tvalid               => axis_pkt_tx_tvalid(I),
            s_axis_tready               => axis_pkt_tx_tready(I),

            -- AXIS Frame output
            m_axis_clk                  => clk_ccn_a(I),
            m_axis_tdata                => axis_tx_tdata,
            m_axis_tkeep                => axis_tx_tkeep,
            m_axis_tlast                => axis_tx_tlast,
            m_axis_tvalid               => axis_tx_tvalid,
            m_axis_tready               => axis_tx_tready,
            m_axis_tuser                => axis_tx_tuser
        );

    end generate gen_ccn;

    -- ============================================================================= --
    -- ==                        UPSTREAM AXIS INTERCONNECT                       == --
    -- ============================================================================= --
    inst_rx_axis_ic: entity work.axis_ic41_rx
    port map (
        aclk                 => axis_clk,
        s00_axis_aclk        => axis_clk,
        s01_axis_aclk        => axis_clk,
        s02_axis_aclk        => axis_clk,
        s03_axis_aclk        => axis_clk,
        m00_axis_aclk        => axis_clk,

        aresetn              => axis_rstn,
        s00_axis_aresetn     => axis_rstn,
        s01_axis_aresetn     => axis_rstn,
        s02_axis_aresetn     => axis_rstn,
        s03_axis_aresetn     => axis_rstn,
        m00_axis_aresetn     => axis_rstn,

        s00_axis_tvalid      => axis_pkt_rx_tvalid_a(0),
        s00_axis_tready      => axis_pkt_rx_tready_a(0),
        s00_axis_tdata       => axis_pkt_rx_tdata_a(0),
        s00_axis_tuser       => axis_pkt_rx_tuser_a(0),
        s00_axis_tid         => C_TID0,

        s01_axis_tvalid      => axis_pkt_rx_tvalid_a(1),
        s01_axis_tready      => axis_pkt_rx_tready_a(1),
        s01_axis_tdata       => axis_pkt_rx_tdata_a(1),
        s01_axis_tuser       => axis_pkt_rx_tuser_a(1),
        s01_axis_tid         => C_TID1,

        s02_axis_tvalid      => axis_pkt_rx_tvalid_a(2),
        s02_axis_tready      => axis_pkt_rx_tready_a(2),
        s02_axis_tdata       => axis_pkt_rx_tdata_a(2),
        s02_axis_tuser       => axis_pkt_rx_tuser_a(2),
        s02_axis_tid         => C_TID2,

        s03_axis_tvalid      => axis_pkt_rx_tvalid_a(3),
        s03_axis_tready      => axis_pkt_rx_tready_a(3),
        s03_axis_tdata       => axis_pkt_rx_tdata_a(3),
        s03_axis_tuser       => axis_pkt_rx_tuser_a(3),
        s03_axis_tid         => C_TID3,

        m00_axis_tvalid      => axis_pkt_rx_tvalid,
        m00_axis_tready      => axis_pkt_rx_tready,
        m00_axis_tdata       => axis_pkt_rx_tdata,
        m00_axis_tid         => axis_pkt_rx_tid,
        m00_axis_tuser(7 downto 0)=> axis_pkt_rx_tuser,
        m00_axis_tuser(9 downto 8)=> open,

        s00_arb_req_suppress => C_LOGIC_ZERO,
        s01_arb_req_suppress => C_LOGIC_ZERO,
        s02_arb_req_suppress => C_LOGIC_ZERO,
        s03_arb_req_suppress => C_LOGIC_ZERO
    );

    -- Drive Tready because corrector is non blocking
    axis_pkt_rx_tready <= '1';

    -- ============================================================================= --
    -- ==                           MATRIX    CORRECTOR                           == --
    -- ============================================================================= --
    inst_matrix_corr: entity work.top_corr_matrix
    port map(
        clk            => axis_clk,
        rst_n          => axis_rstn,

        pps            => pps,

        -- AXI-MM interface
        s_axi_m2s      => addrmap_o.corr_matrix_0,
        s_axi_s2m      => addrmap_i.corr_matrix_0,

        -- AXIS input
        s_axis_tdata   => axis_pkt_rx_tdata,
        s_axis_tuser   => axis_pkt_rx_tuser,
        s_axis_tvalid  => axis_pkt_rx_tvalid,

        -- AXIS output
        m_axis_tdata   => axis_corr_tdata,
        m_axis_tuser   => axis_corr_tuser,
        m_axis_tvalid  => axis_corr_tvalid
    );

    -- ============================================================================= --
    -- ==                           PSC WAVEFORM GENERATOR                        == --
    -- ============================================================================= --

    inst_pscgen: entity work.top_pscgen
    generic map(
        G_W_TUSER          => 8
    )
    port map(
        clk                 => axis_clk,
        rstn                => axis_rstn,
        -- AXI MM interface
        s_axi_m2s       => addrmap_o.pscgen_0,
        s_axi_s2m       => addrmap_i.pscgen_0,
        -- AXIS input
        s_axis_tdata_pscid  => axis_corr_tdata(23 downto 16),
        s_axis_tdata_value  => axis_corr_tdata(15 downto 0),
        s_axis_tuser        => axis_corr_tuser,
        s_axis_tvalid       => axis_corr_tvalid,
        -- AXIS output
        m_axis_tdata_pscid  => axis_pscgen_tdata_pscid,
        m_axis_tdata_value  => axis_pscgen_tdata_value,
        m_axis_tuser        => axis_pscgen_tuser,
        m_axis_tvalid       => axis_pscgen_tvalid
    );



    -- Throttling
    inst_downstream_fifo : axis_downstream_fifo
    port map (
        s_axis_aresetn => axis_rstn,
        s_axis_aclk => axis_clk,
        s_axis_tvalid => axis_pscgen_tvalid,
        s_axis_tready => open,      -- no back pressure possible on this side
        s_axis_tdata(15 downto 0) => axis_pscgen_tdata_value,
        s_axis_tdata(23 downto 16) => axis_pscgen_tdata_pscid,
        s_axis_tuser => axis_pscgen_tuser,
        m_axis_tvalid => axis_corrfifo_tvalid,
        m_axis_tready =>  axis_corrfifo_tready,
        m_axis_tdata =>  axis_corrfifo_tdata_sm,
        m_axis_tuser =>  axis_corrfifo_tuser,

        almost_full => open -- TODO monitor to detect overrun
    );


    -- ============================================================================= --
    -- ==                        DOWNSTREAM AXIS BROADCASTER                      == --
    -- ============================================================================= --

    -- Temporary size matching for corrector
    axis_corrfifo_tdata(31 downto 0)    <= std_logic_vector(resize(signed(axis_corrfifo_tdata_sm(15 downto 0)), 32));
    axis_corrfifo_tdata(39 downto 32)   <= axis_corrfifo_tdata_sm(23 downto 16);
    axis_corrfifo_tdata(47 downto 40)   <= (others => '0');

    inst_tx_axis_bc: entity work.axis_bc14_tx
    port map(
        aclk            => axis_clk,
        aresetn         => axis_rstn,
        s_axis_tvalid   => axis_corrfifo_tvalid,
        s_axis_tready   => axis_corrfifo_tready,
        s_axis_tdata    => axis_corrfifo_tdata,
        s_axis_tuser    => axis_corrfifo_tuser,
        m_axis_tvalid   => axis_pkt_tx_tvalid,
        m_axis_tready   => axis_pkt_tx_tready,
        m_axis_tdata    => axis_pkt_tx_tdata,
        m_axis_tuser    => axis_pkt_tx_tuser
    );

    -- ============================================================================= --
    -- ==                           PPS                                           == --
    -- ============================================================================= --
    inst_pps:entity work.pps_cnt
    generic map(
        G_CLK_FREQ  => 100000000,
        G_W_TS      => timeref'length
    )
    port map(
        clk             => pi_payload.m_axi4l_reg_aclk,
        rst_n           => pi_payload.m_axi4l_reg_areset_n,
        srst            => '0',
        pps             => pps,
        timestamp       => timeref
    );
    mc_time <= timeref(mc_time'left downto 0);

    -- ============================================================================= --
    -- ==                          DAQ SYSTEM                                     == --
    -- ============================================================================= --

    -- Temporary source: trigger on new bpm seq
    p_trig_seq:process(axis_clk, axis_rstn)
    begin
        if axis_rstn = '0' then
            r_last_seq          <= (others => '0');
            axis_daqs00_tvalid  <= '0';

            d1_axis_daqs01_tdata    <= (others => '0');
            d1_axis_daqs02_tdata    <= (others => '0');
            d1_axis_daqs01_tvalid   <= '0';
            d1_axis_daqs02_tvalid   <= '0';

            d2_axis_daqs01_tdata    <= (others => '0');
            d2_axis_daqs02_tdata    <= (others => '0');
            d2_axis_daqs01_tvalid   <= '0';
            d2_axis_daqs02_tvalid   <= '0';

        elsif rising_edge(axis_clk) then
            if (axis_pkt_rx_tvalid and axis_pkt_rx_tready) = '1' then
                r_last_seq  <= axis_pkt_rx_tuser(7 downto 0);

                if r_last_seq /= axis_pkt_rx_tuser(7 downto 0) then
                    axis_daqs00_tvalid  <= '1';
                else
                    axis_daqs00_tvalid  <= '0';
                end if;
            else
                axis_daqs00_tvalid      <= '0';
            end if;

            d1_axis_daqs01_tdata    <= axis_daqs01_tdata;
            d1_axis_daqs01_tvalid   <= axis_daqs01_tvalid;
            d1_axis_daqs02_tdata    <= axis_daqs02_tdata;
            d1_axis_daqs02_tvalid   <= axis_daqs02_tvalid;

            d2_axis_daqs01_tdata    <= d1_axis_daqs01_tdata;
            d2_axis_daqs01_tvalid   <= d1_axis_daqs01_tvalid;
            d2_axis_daqs02_tdata    <= d1_axis_daqs02_tdata;
            d2_axis_daqs02_tvalid   <= d1_axis_daqs02_tvalid;

        end if;
    end process;

    axis_daqs00_tdata(63 downto 48) <= x"8005";                                 -- Trigger source, new bpm seq event
    axis_daqs00_tdata(47 downto 32) <= x"00"&axis_pkt_rx_tuser(7 downto 0);     -- Ancillary is seq num
    axis_daqs00_tdata(31 downto 0)  <= timeref(31 downto 0);                    -- Timestamp

    axis_daqs01_tdata(63 downto 56) <= x"40";                                   -- BPM x source
    axis_daqs01_tdata(55 downto 48) <= axis_pkt_rx_tdata(71 downto 64);         -- BPM id
    axis_daqs01_tdata(47 downto 32) <= x"00"&axis_pkt_rx_tuser(7 downto 0);     -- Ancillary is seq num
    axis_daqs01_tdata(31 downto 0)  <= axis_pkt_rx_tdata(31 downto 0);          -- Position
    axis_daqs01_tvalid              <= axis_pkt_rx_tvalid and axis_pkt_rx_tready;

    axis_daqs02_tdata(63 downto 56) <= x"42";                                   -- BPM y source
    axis_daqs02_tdata(55 downto 48) <= axis_pkt_rx_tdata(71 downto 64);         -- BPM id
    axis_daqs02_tdata(47 downto 32) <= x"00"&axis_pkt_rx_tuser(7 downto 0);     -- Ancillary is seq num
    axis_daqs02_tdata(31 downto 0)  <= axis_pkt_rx_tdata(63 downto 32);         -- Position
    axis_daqs02_tvalid              <= axis_pkt_rx_tvalid and axis_pkt_rx_tready;

    axis_daqs03_tdata(63 downto 56) <= x"B0";                                   -- PSC source
    axis_daqs03_tdata(55 downto 48) <= axis_pscgen_tdata_pscid;                 -- PSC id
    axis_daqs03_tdata(47 downto 32) <= x"00"&axis_pscgen_tuser;                 -- Ancillary is seq num
    axis_daqs03_tdata(31 downto 16) <= (others => axis_pscgen_tdata_value(15)); -- Sign extension
    axis_daqs03_tdata(15 downto 0)  <= axis_pscgen_tdata_value;                 -- Value
    axis_daqs03_tvalid              <= axis_pscgen_tvalid;

    -- DAQ interconnect data source
    inst_daq_axis_ic: entity work.axis_ic_capt
    port map (
        aclk                 => axis_clk,
        s00_axis_aclk        => axis_clk,
        s01_axis_aclk        => axis_clk,
        s02_axis_aclk        => axis_clk,
        s03_axis_aclk        => axis_clk,
        m00_axis_aclk        => axis_clk,

        aresetn              => axis_rstn,
        s00_axis_aresetn     => axis_rstn,
        s01_axis_aresetn     => axis_rstn,
        s02_axis_aresetn     => axis_rstn,
        s03_axis_aresetn     => axis_rstn,
        m00_axis_aresetn     => axis_rstn,

        s00_axis_tvalid      => axis_daqs00_tvalid,
        s00_axis_tready      => open, --no back pressure
        s00_axis_tdata       => axis_daqs00_tdata,

        s01_axis_tvalid      => d2_axis_daqs01_tvalid,
        s01_axis_tready      => open, --no back pressure
        s01_axis_tdata       => d2_axis_daqs01_tdata,

        s02_axis_tvalid      => d2_axis_daqs02_tvalid,
        s02_axis_tready      => open, --no back pressure
        s02_axis_tdata       => d2_axis_daqs02_tdata,

        s03_axis_tvalid      => axis_daqs03_tvalid,
        s03_axis_tready      => open, --no back pressure
        s03_axis_tdata       => axis_daqs03_tdata,

        m00_axis_tvalid      => axis_daqm00_tvalid,
        m00_axis_tready      => axis_daqm00_tready,
        m00_axis_tdata       => axis_daqm00_tdata,

        s00_arb_req_suppress => C_LOGIC_ZERO,
        s01_arb_req_suppress => C_LOGIC_ZERO,
        s02_arb_req_suppress => C_LOGIC_ZERO,
        s03_arb_req_suppress => C_LOGIC_ZERO
    );

    axis_daqm00_tready  <= '1'; -- no back pressure from DAQ

    inst_daq: entity work.daq_top
    generic map (
        g_fifo_arch                => "ULTRASCALE",
        g_input_data_channel_count => daq_data'length,
        g_axi_data_width           => po_payload.s_axi4_daq.wdata'length,
        g_ext_str_ena              => "11"

    )
    port map (
        pi_clock             => axis_clk,
        pi_reset             => axis_rst,
        pi_s_reg_reset       => rst,
        pi_s_reg_if          => addrmap_o.daq_0,
        po_s_reg_if          => addrmap_i.daq_0,
        pi_m_axi4_aclk       => pi_payload.s_axi4_daq_aclk,
        pi_m_axi4_areset_n   => pi_payload.s_axi4_daq_areset_n,
        pi_m_axi4            => m_axi4_daq_s2m,
        po_m_axi4            => m_axi4_daq_m2s,
        -- Triggers and Strobe
        pi_trg(0)            => daq_trigger,
        pi_trg(1)            => daq_trigger,
        pi_ext_str           => daq_strobe,
        pi_transaction_end(0)=> daq_stop,
        pi_transaction_end(1)=> daq_stop,
        po_buffer_start      => daq_irq,
        -- Data
        pi_data              => daq_data,
        --
        pi_pulse_number      => timeref(31 downto 0)
    );

    m_axi4_daq_s2m <= f_common_to_bsp(pi_payload.s_axi4_daq);
    po_payload.s_axi4_daq <= f_common_to_bsp(m_axi4_daq_m2s);

    daq_data(0)                 <= axis_daqm00_tdata(31 downto 0);
    daq_data(1)                 <= axis_daqm00_tdata(63 downto 32);

    daq_strobe(0)   <= axis_daqm00_tvalid;
    daq_strobe(1)   <= axis_daqm00_tvalid;

    daq_stop   <= addrmap_o.daq_control.stop.data(0);

    p_daq_trigger:process(pi_payload.m_axi4l_reg_aclk, pi_payload.m_axi4l_reg_areset_n)
    begin
        if pi_payload.m_axi4l_reg_areset_n = '0' then
            daq_trigger     <= '0';
            r_daq_trigger   <= (others => '0');
        elsif rising_edge(pi_payload.m_axi4l_reg_aclk) then
            r_daq_trigger   <= r_daq_trigger(0) & addrmap_o.daq_control.trigger.data(0);
            daq_trigger     <= (not r_daq_trigger(1)) and r_daq_trigger(0);
        end if;
    end process;


    -- IRQ vector
    po_payload.ps_irq_req(1 downto 0)   <= daq_irq;
    po_payload.ps_irq_req(2)            <= daq_trigger;  --for test
    po_payload.ps_irq_req(7 downto 3)   <= (others => '0');


end architecture struct;
