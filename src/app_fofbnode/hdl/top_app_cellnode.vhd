library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.or_reduce;

library unisim;
use unisim.vcomponents.all;

library xpm;
use xpm.vcomponents.all;

library desy;
use desy.common_to_desyrdl.all;
use desy.common_bsp_ifs.all;
use desy.common_axi.t_axi4_m2s;
use desy.common_axi.t_axi4_s2m;
use desy.common_types.t_32b_slv_vector;

library desyrdl;
use desyrdl.common.all;
use desyrdl.pkg_app_fofb_cellnode.all;
use desyrdl.pkg_comlbp.t_comlbp_m2s;
use desyrdl.pkg_comlbp.t_comlbp_s2m;

use work.pkg_bsp_fmc2zup_payload.all;
use work.pkg_app_fofb_cellnode.all;

library ccn_eth_lib;
library comlbp_lib;

entity bsp_fmc2zup_payload is
    port (
        pi_payload  : in    t_payload_i;
        po_payload  : out   t_payload_o;
        pio_payload : inout t_payload_io
    );
end entity bsp_fmc2zup_payload;


architecture struct of bsp_fmc2zup_payload is

    -- Some dumb constant
    constant c_std_zero         : std_logic := '0';
    constant c_std_one          : std_logic := '0';
    constant c_slv_zero         : std_logic_vector(7 downto 0) := (others => '0');
    constant c_slv_one          : std_logic_vector(7 downto 0) := (others => '1');

    type arr_slv is array (natural range <>) of std_logic_vector;
    type t_a_comlbp_m2s is array (0 to 3) of t_comlbp_m2s;
    type t_a_comlbp_s2m is array (0 to 3) of t_comlbp_s2m;

    ------------------------
    -- SIGNAL DECLARATION --
    ------------------------

    -- AXI-MM Arbiter
    signal addrmap_i            : t_addrmap_app_fofb_cellnode_in;
    signal addrmap_o            : t_addrmap_app_fofb_cellnode_out;
    signal s_reg_if_m2s         : t_app_fofb_cellnode_m2s;
    signal s_reg_if_s2m         : t_app_fofb_cellnode_s2m;

    -- FMC1 (4SFP+) signals
    signal fmc1_sfp_tx_n        : std_logic_vector(3 downto 0);
    signal fmc1_sfp_tx_p        : std_logic_vector(3 downto 0);
    signal fmc1_sfp_rx_n        : std_logic_vector(3 downto 0);
    signal fmc1_sfp_rx_p        : std_logic_vector(3 downto 0);
    signal fmc1_sfp_rxlos       : std_logic_vector(3 downto 0);
    signal fmc1_sfp_mod_abs     : std_logic_vector(3 downto 0);
    signal fmc1_sfp_tx_disable  : std_logic_vector(3 downto 0);
    signal fmc1_sfp_tx_fault    : std_logic_vector(3 downto 0);
    signal fmc1_ref_clk_n       : std_logic;
    signal fmc1_ref_clk_p       : std_logic;

    -- FMC2 (4SFP+) signals
    signal fmc2_sfp_tx_n        : std_logic_vector(3 downto 0);
    signal fmc2_sfp_tx_p        : std_logic_vector(3 downto 0);
    signal fmc2_sfp_rx_n        : std_logic_vector(3 downto 0);
    signal fmc2_sfp_rx_p        : std_logic_vector(3 downto 0);
    signal fmc2_sfp_rxlos       : std_logic_vector(3 downto 0);
    signal fmc2_sfp_mod_abs     : std_logic_vector(3 downto 0);
    signal fmc2_sfp_tx_disable  : std_logic_vector(3 downto 0);
    signal fmc2_sfp_tx_fault    : std_logic_vector(3 downto 0);
    signal fmc2_ref_clk_n       : std_logic;
    signal fmc2_ref_clk_p       : std_logic;

    -- QPLL common quad signals
    signal gth_qpll0reset       : std_logic;
    signal gth_qpll0lock        : std_logic;
    signal gth_qpll0outclk      : std_logic;
    signal gth_qpll0refclk      : std_logic;
    signal gth_qpll0fblost      : std_logic;
    signal gth_qpll0reflost     : std_logic;
    signal gth_qpll1reset       : std_logic;
    signal gth_qpll1lock        : std_logic;
    signal gth_qpll1outclk      : std_logic;
    signal gth_qpll1refclk      : std_logic;
    signal gth_qpll1fblost      : std_logic;
    signal gth_qpll1reflost     : std_logic;

    signal gty_qpll0reset       : std_logic;
    signal gty_qpll0lock        : std_logic;
    signal gty_qpll0outclk      : std_logic;
    signal gty_qpll0refclk      : std_logic;
    signal gty_qpll0fblost      : std_logic;
    signal gty_qpll0reflost     : std_logic;
    signal gty_qpll1reset       : std_logic;
    signal gty_qpll1lock        : std_logic;
    signal gty_qpll1outclk      : std_logic;
    signal gty_qpll1refclk      : std_logic;
    signal gty_qpll1fblost      : std_logic;
    signal gty_qpll1reflost     : std_logic;

    -- COMBPM signals
    signal clk_combpm               : std_logic;
    signal axis_combpm_tdata_posx   : std_logic_vector(31 downto 0);
    signal axis_combpm_tdata_posy   : std_logic_vector(31 downto 0);
    signal axis_combpm_tdata_seq    : std_logic_vector(15 downto 0);
    signal axis_combpm_tdata_bpmid  : std_logic_vector(15 downto 0);
    signal axis_combpm_tdata        : std_logic_vector(95 downto 0);
    signal axis_combpm_tvalid       : std_logic;
    signal axis_combpm_sync_tvalid  : std_logic;
    signal axis_combpm_sync_tready  : std_logic;
    signal axis_combpm_sync_tdata   : std_logic_vector(95 downto 0);

    -- COMLBP signals
    signal comlbp_am2s : t_a_comlbp_m2s;
    signal comlbp_as2m : t_a_comlbp_s2m;

    signal comlbp_qpllreset          : std_logic_vector(3 downto 0);
    signal axis_comlbp_sync_tready   : std_logic_vector(3 downto 0);
    signal axis_comlbp_sync_tvalid   : std_logic_vector(3 downto 0);
    signal axis_comlbp_sync_tdata    : arr_slv(0 to 3)(95 downto 0);

    -- COMCELLNODE signals
    signal clk_comcellnode      : std_logic;

    signal axis_tx_tvalid       : std_logic;
    signal axis_tx_tready       : std_logic;
    signal axis_tx_tdata        : std_logic_vector(63 downto 0);
    signal axis_tx_tkeep        : std_logic_vector(7 downto 0);
    signal axis_tx_tlast        : std_logic;
    signal axis_tx_tuser        : std_logic_vector(0 downto 0);

    signal axis_rx_tvalid       : std_logic;
    signal axis_rx_tready       : std_logic;
    signal axis_rx_tuser        : std_logic_vector(0 downto 0);
    signal axis_rx_tlast        : std_logic;
    signal axis_rx_tkeep        : std_logic_vector(7 downto 0);
    signal axis_rx_tdata        : std_logic_vector(63 downto 0);

    -- PACKET STREAM
    signal axis_pkt_rx_tvalid   : std_logic;
    signal axis_pkt_rx_tready   : std_logic;
    signal axis_pkt_rx_tlast    : std_logic;
    signal axis_pkt_rx_tuser    : std_logic_vector(7 downto 0);
    signal axis_pkt_rx_tdata    : std_logic_vector(47 downto 0);

    signal axis_pkt_tx_tready   : std_logic;
    signal axis_pkt_tx_tvalid   : std_logic;
    signal axis_pkt_tx_tdata    : std_logic_vector(95 downto 0);

    -- PSCGEN
    signal axis_pscgen_tdata_value    : std_logic_vector(15 downto 0);
    signal axis_pscgen_tdata_pscid    : std_logic_vector(7 downto 0);
    signal axis_pscgen_tuser    : std_logic_vector(7 downto 0);
    signal axis_pscgen_tvalid   : std_logic;


    -- COMCORR STREAM
    signal axis_comcorr_tdata   : std_logic_vector(47 downto 0);
    signal axis_comcorr_tuser   : std_logic_vector(7 downto 0);
    signal axis_comcorr_tvalid  : std_logic;

    -- RTM signals
    signal rtm_tx               : std_logic_vector(31 downto 0);
    signal rtm_tx_en            : std_logic_vector(7 downto 0);

    -- SYNCHRO signals
    signal pps                  : std_logic;
    signal mc_time              : std_logic_vector(39 downto 0);
    signal timeref              : std_logic_vector(63 downto 0);

    -- DAQ
    signal m_axi4_daq_s2m       : t_axi4_s2m;
    signal m_axi4_daq_m2s       : t_axi4_m2s;
    signal daq_strobe           : std_logic_vector(1 downto 0);
    signal daq_stop             : std_logic;
    signal daq_trigger          : std_logic;
    signal r_daq_trigger        : std_logic_vector(1 downto 0);
    signal daq_data             : t_32b_slv_vector(4 downto 0);

    signal ts_strobe_0          : std_logic_vector(31 downto 0);
    signal ts_strobe_1          : std_logic_vector(31 downto 0);
    signal rs_strobe_0          : std_logic;
    signal rs_strobe_1          : std_logic;

    -- Other signals
    signal rst                  : std_logic;

begin

    rst     <= not pi_payload.m_axi4l_reg_areset_n;

    ------------------------------
    -- Assign not used BSP signals
    ------------------------------
    -- MLVDS
    po_payload.mlvds    <= x"00";
    po_payload.mlvds_oe <= x"00";

    -- place buffer and assign IO standard
    inst_fmc1_sync_buf   : OBUFDS
    generic map (IOSTANDARD => "LVDS")
    port map( O => po_payload.fmc1.sync_p, OB =>  po_payload.fmc1.sync_n , I => '0');

    inst_fmc1_refclk_buf : OBUFDS
    generic map (IOSTANDARD => "LVDS")
    port map( O => po_payload.fmc1.refclk_p, OB =>  po_payload.fmc1.refclk_n , I => '0');

    --------------------
    -- AXI-MM ARBITER --
    --------------------
    s_reg_if_m2s            <= f_common_to_desyrdl(pi_payload.m_axi4l_reg);
    po_payload.m_axi4l_reg  <= f_common_to_desyrdl(s_reg_if_s2m);

    inst_app_desyrdl : entity desyrdl.app_fofb_cellnode
    port map (
        pi_clock     => pi_payload.m_axi4l_reg_aclk,
        pi_reset     => rst,
        pi_s_top     => s_reg_if_m2s,
        po_s_top     => s_reg_if_s2m,
        pi_addrmap   => addrmap_i,
        po_addrmap   => addrmap_o
    );


    -- ============================================================================= --
    -- ==       DATA PROCESSING CHAIN: UPSTREAM, FROM BPMS TO CENTRAL NODE        == --
    -- ============================================================================= --
    -- Modules are listed in the signal processing order

    -----------------------
    -- FMC 4SFP+ CAENELS --
    -----------------------
    inst_fmc1_4sfp: entity work.fmcbsp_4sfp
    port map(
        -- == == FMC SIDE == == --
        -- Transceivers clocks
        fmc_gbtclk0_p                              => pi_payload.fmc1_mgt.clk_p(0),
        fmc_gbtclk0_n                              => pi_payload.fmc1_mgt.clk_n(0),

        -- LA pins, connect all but uses a few
        fmc_la_p                                   => pio_payload.fmc1.la_p,
        fmc_la_n                                   => pio_payload.fmc1.la_n,

        -- MGT differential pairs, only fours used
        fmc_dp_m2c_p                               => pi_payload.fmc1_mgt.dp_p(3 downto 0),
        fmc_dp_m2c_n                               => pi_payload.fmc1_mgt.dp_n(3 downto 0),
        fmc_dp_c2m_p                               => po_payload.fmc1_mgt.dp_p(3 downto 0),
        fmc_dp_c2m_n                               => po_payload.fmc1_mgt.dp_n(3 downto 0),

        -- == == LOGIC SIDE == == --
        osc_i2c_scl                                => '0',
        osc_i2c_sda                                => open,
        osc_ref_clk_p                              => fmc1_ref_clk_p,
        osc_ref_clk_n                              => fmc1_ref_clk_n,
        sfp_tx_n                                   => fmc1_sfp_tx_n,
        sfp_tx_p                                   => fmc1_sfp_tx_p,
        sfp_rx_n                                   => fmc1_sfp_rx_n,
        sfp_rx_p                                   => fmc1_sfp_rx_p,
        sfp_rxlos                                  => fmc1_sfp_rxlos,
        sfp_mod_abs                                => fmc1_sfp_mod_abs,
        sfp_tx_disable                             => fmc1_sfp_tx_disable,
        sfp_tx_fault                               => fmc1_sfp_tx_fault,
        sfp_rs0                                    => (others => '0'),
        sfp_rs1                                    => (others => '0'),
        sfp_i2c_scl                                => (others => '0'),
        sfp_i2c_sda                                => open
    );
    fmc1_sfp_tx_disable(3 downto  1) <= "000";

    inst_fmc2_4sfp: entity work.fmcbsp_4sfp
    port map(
        -- == == FMC SIDE == == --
        -- Transceivers clocks
        fmc_gbtclk0_p                              => pi_payload.fmc2_mgt.clk_p(0),
        fmc_gbtclk0_n                              => pi_payload.fmc2_mgt.clk_n(0),

        -- LA pins, connect all but uses a few
        fmc_la_p                                   => pio_payload.fmc2.la_p,
        fmc_la_n                                   => pio_payload.fmc2.la_n,

        -- MGT differential pairs, only fours used
        fmc_dp_m2c_p                               => pi_payload.fmc2_mgt.dp_p(3 downto 0),
        fmc_dp_m2c_n                               => pi_payload.fmc2_mgt.dp_n(3 downto 0),
        fmc_dp_c2m_p                               => po_payload.fmc2_mgt.dp_p(3 downto 0),
        fmc_dp_c2m_n                               => po_payload.fmc2_mgt.dp_n(3 downto 0),

        -- == == LOGIC SIDE == == --
        osc_i2c_scl                                => '0',
        osc_i2c_sda                                => open,
        osc_ref_clk_p                              => fmc2_ref_clk_p,
        osc_ref_clk_n                              => fmc2_ref_clk_n,
        sfp_tx_n                                   => fmc2_sfp_tx_n,
        sfp_tx_p                                   => fmc2_sfp_tx_p,
        sfp_rx_n                                   => fmc2_sfp_rx_n,
        sfp_rx_p                                   => fmc2_sfp_rx_p,
        sfp_rxlos                                  => fmc2_sfp_rxlos,
        sfp_mod_abs                                => fmc2_sfp_mod_abs,
        sfp_tx_disable                             => fmc2_sfp_tx_disable,
        sfp_tx_fault                               => fmc2_sfp_tx_fault,
        sfp_rs0                                    => (others => '0'),
        sfp_rs1                                    => (others => '0'),
        sfp_i2c_scl                                => (others => '0'),
        sfp_i2c_sda                                => open
    );
    fmc2_sfp_tx_disable(3 downto  2) <= "00";

    ------------------------
    -- GTHE COMMON (QPLL) --
    ------------------------
    inst_gthe_common: entity work.gthe_common
    port map(
        -- Differential clock intput
        gtrefclk_p      => fmc2_ref_clk_p,
        gtrefclk_n      => fmc2_ref_clk_n,

        -- Detection features (100 MHz clk)
        freerun_clk     => pi_payload.ps_clk0,

        -- Buffered ref
        buff_gtrefclk   => open,

        -- QPLL0 interface (10GbE)
        qpll0reset      => gth_qpll0reset,
        qpll0lock       => gth_qpll0lock,
        qpll0outclk     => gth_qpll0outclk,
        qpll0refclk     => gth_qpll0refclk,
        qpll0fblost     => gth_qpll0fblost,
        qpll0reflost    => gth_qpll0reflost,

        -- QPLL1 interface (COMBPM)
        qpll1reset      => gth_qpll1reset,
        qpll1lock       => gth_qpll1lock,
        qpll1outclk     => gth_qpll1outclk,
        qpll1refclk     => gth_qpll1refclk,
        qpll1fblost     => gth_qpll1fblost,
        qpll1reflost    => gth_qpll1reflost
    );


    ------------------------
    -- GTYE COMMON (QPLL) --
    ------------------------
    inst_gtye_common: entity work.gtye_common
    port map(
        -- Differential clock intput
        gtrefclk_p      => fmc1_ref_clk_p,
        gtrefclk_n      => fmc1_ref_clk_n,

        -- Detection features (100 MHz clk)
        freerun_clk     => pi_payload.ps_clk0,

        -- Buffered ref
        buff_gtrefclk   => open,

        -- QPLL0 interface (10GbE)
        qpll0reset      => gty_qpll0reset,
        qpll0lock       => gty_qpll0lock,
        qpll0outclk     => gty_qpll0outclk,
        qpll0refclk     => gty_qpll0refclk,
        qpll0fblost     => gty_qpll0fblost,
        qpll0reflost    => gty_qpll0reflost,

        -- QPLL1 interface (COMLBP)
        qpll1reset      => gty_qpll1reset,
        qpll1lock       => gty_qpll1lock,
        qpll1outclk     => gty_qpll1outclk,
        qpll1refclk     => gty_qpll1refclk,
        qpll1fblost     => gty_qpll1fblost,
        qpll1reflost    => gty_qpll1reflost
    );

    gty_qpll1reset <= or_reduce(comlbp_qpllreset);

    ------------
    -- COMBPM --
    ------------
    inst_combpm: entity work.top_combpm_electron
    port map(
        rst_n                         => pi_payload.m_axi4l_reg_areset_n,
        free_100_clk                  => pi_payload.ps_clk1,
        pps                           => pps,
        mc_time                       => mc_time,

        -- Transceiver QPLL interface
        qpll_out_clk                  => gth_qpll1outclk,
        qpll_ref_clk                  => gth_qpll1refclk,
        qpll_reset                    => gth_qpll1reset,
        qpll_lock                     => gth_qpll1lock,

        -- Debug output
        debug_datarx                  => open,
        debug_status                  => open,
        error_detect                  => open,

        -- SFP interfaces
        sfp_txp                       => fmc2_sfp_tx_p(0),
        sfp_txn                       => fmc2_sfp_tx_n(0),
        sfp_rxp                       => fmc2_sfp_rx_p(0),
        sfp_rxn                       => fmc2_sfp_rx_n(0),
        sfp_rx_los                    => fmc2_sfp_rxlos(0),
        sfp_mod_abs                   => fmc2_sfp_mod_abs(0),
        sfp_tx_disable                => fmc2_sfp_tx_disable(0),
        sfp_tx_fault                  => fmc2_sfp_tx_fault(0),

        -- AXIS interface
        m_axis_aclk                   => clk_combpm,
        m_axis_tdest                  => open,
        m_axis_tdata_posx             => axis_combpm_tdata_posx,
        m_axis_tdata_posy             => axis_combpm_tdata_posy,
        m_axis_tdata_bpmid            => axis_combpm_tdata_bpmid,
        m_axis_tdata_seq              => axis_combpm_tdata_seq,
        m_axis_tvalid                 => axis_combpm_tvalid,

        -- AXI bus interface
        s_axi_aclk                    => pi_payload.m_axi4l_reg_aclk,
        s_axi_m2s                     => addrmap_o.combpm_0,
        s_axi_s2m                     => addrmap_i.combpm_0
    );

    axis_combpm_tdata <= axis_combpm_tdata_seq & axis_combpm_tdata_bpmid & axis_combpm_tdata_posy & axis_combpm_tdata_posx;

    inst_upstream_lbp_fifo : xpm_fifo_axis
    generic map (
       CLOCKING_MODE       => "independent_clock",
       FIFO_DEPTH          => 64,
       RD_DATA_COUNT_WIDTH => 7,
       WR_DATA_COUNT_WIDTH => 7,
       RELATED_CLOCKS      => 0,
       TDATA_WIDTH         => 96
    )
    port map (
       s_aresetn           => pi_payload.m_axi4l_reg_areset_n,
       almost_empty_axis   => open,
       almost_full_axis    => open,
       dbiterr_axis        => open,
       sbiterr_axis        => open,
       prog_empty_axis     => open,
       prog_full_axis      => open,
       rd_data_count_axis  => open,
       wr_data_count_axis  => open,
       injectdbiterr_axis  => '0',
       injectsbiterr_axis  => '0',

       s_aclk              => clk_combpm,
       s_axis_tvalid       => axis_combpm_tvalid,
       s_axis_tdata        => axis_combpm_tdata,
       s_axis_tready       => open,
       s_axis_tdest        => (others => '0'),
       s_axis_tid          => (others => '0'),
       s_axis_tkeep        => (others => '0'),
       s_axis_tlast        => '0',
       s_axis_tstrb        => (others => '0'),
       s_axis_tuser        => (others => '0'),

       m_aclk              => pi_payload.m_axi4l_reg_aclk,
       m_axis_tdata        => axis_combpm_sync_tdata,
       m_axis_tready       => axis_combpm_sync_tready,
       m_axis_tvalid       => axis_combpm_sync_tvalid
       --m_axis_tdest        => open,
       --m_axis_tid          => open,
       --m_axis_tkeep        => open,
       --m_axis_tlast        => open,
       --m_axis_tstrb        => open,
       --m_axis_tuser        => open
    );


    ------------
    -- COMLBP --
    ------------
    -- Generate four instances

    G_COMLBP: for I in 0 to 3 generate

        signal clk_comlbp               : std_logic;
        signal axis_comlbp_tdata_posx   : std_logic_vector(31 downto 0);
        signal axis_comlbp_tdata_posy   : std_logic_vector(31 downto 0);
        signal axis_comlbp_tdata_faseq  : std_logic_vector(15 downto 0);
        signal axis_comlbp_tdata_bpmid  : std_logic_vector(15 downto 0);
        signal axis_comlbp_tdata        : std_logic_vector(95 downto 0);
        signal axis_comlbp_tvalid       : std_logic;

    begin

        inst_comlbp: entity comlbp_lib.top_comlbp
        generic map(
            G_IPNUM                 => I
        )
        port map(
            aresetn                 => pi_payload.m_axi4l_reg_areset_n,
            free_100_clk            => pi_payload.ps_clk1,
            pps                     => pps,

            -- Common Quad signals
            qpll_clk_in             => gty_qpll1outclk,
            qpll_refclk_in          => gty_qpll1refclk,
            qpll_lock_in            => gty_qpll1lock,
            qpll_reset_out          => comlbp_qpllreset(I),
            qpll_fblost_in          => gty_qpll1fblost,
            qpll_reflost_in         => gty_qpll1reflost,

            -- SFP interfaces
            sfp_txp                 => fmc1_sfp_tx_p(I),
            sfp_txn                 => fmc1_sfp_tx_n(I),
            sfp_rxp                 => fmc1_sfp_rx_p(I),
            sfp_rxn                 => fmc1_sfp_rx_n(I),
            sfp_rx_los              => fmc1_sfp_rxlos(I),
            sfp_mod_abs             => fmc1_sfp_mod_abs(I),
            sfp_tx_disable          => fmc1_sfp_tx_disable(I),
            sfp_tx_fault            => fmc1_sfp_tx_fault(I),

            dbg_data                => open,
            dbg_valid               => open,

            -- AXIS output
            m_axis_aclk             => clk_comlbp,
            m_axis_tvalid           => axis_comlbp_tvalid,
            m_axis_tdata_xpos       => axis_comlbp_tdata_posx,
            m_axis_tdata_ypos       => axis_comlbp_tdata_posy,
            m_axis_tdata_faseq      => axis_comlbp_tdata_faseq,
            m_axis_tdata_bpmid      => axis_comlbp_tdata_bpmid,

            -- AXI-MM interface
            s_axi_clk               => pi_payload.m_axi4l_reg_aclk,
            s_axi_m2s               => comlbp_am2s(I),
            s_axi_s2m               => comlbp_as2m(I)
        );


        --------------------------------
        -- UPSTREAM AXIS INTERCONNECT --
        --------------------------------

        axis_comlbp_tdata <= axis_comlbp_tdata_faseq & axis_comlbp_tdata_bpmid & axis_comlbp_tdata_posy & axis_comlbp_tdata_posx;

        inst_upstream_lbp_fifo : xpm_fifo_axis
        generic map (
           CLOCKING_MODE       => "independent_clock",
           FIFO_DEPTH          => 64,
           RD_DATA_COUNT_WIDTH => 7,
           WR_DATA_COUNT_WIDTH => 7,
           RELATED_CLOCKS      => 0,
           TDATA_WIDTH         => 96
        )
        port map (
           s_aresetn           => pi_payload.m_axi4l_reg_areset_n,
           almost_empty_axis   => open,
           almost_full_axis    => open,
           dbiterr_axis        => open,
           sbiterr_axis        => open,
           prog_empty_axis     => open,
           prog_full_axis      => open,
           rd_data_count_axis  => open,
           wr_data_count_axis  => open,
           injectdbiterr_axis  => '0',
           injectsbiterr_axis  => '0',

           s_aclk              => clk_comlbp,
           s_axis_tvalid       => axis_comlbp_tvalid,
           s_axis_tdata        => axis_comlbp_tdata,
           s_axis_tready       => open,
           s_axis_tdest        => (others => '0'),
           s_axis_tid          => (others => '0'),
           s_axis_tkeep        => (others => '0'),
           s_axis_tlast        => '0',
           s_axis_tstrb        => (others => '0'),
           s_axis_tuser        => (others => '0'),

           m_aclk              => pi_payload.m_axi4l_reg_aclk,
           m_axis_tdata        => axis_comlbp_sync_tdata(I),
           m_axis_tready       => axis_comlbp_sync_tready(I),
           m_axis_tvalid       => axis_comlbp_sync_tvalid(I)
           --m_axis_tdest        => open,
           --m_axis_tid          => open,
           --m_axis_tkeep        => open,
           --m_axis_tlast        => open,
           --m_axis_tstrb        => open,
           --m_axis_tuser        => open
        );

    end generate G_COMLBP;

    comlbp_am2s(0)  <= addrmap_o.comlbp_0;
    comlbp_am2s(1)  <= addrmap_o.comlbp_1;
    comlbp_am2s(2)  <= addrmap_o.comlbp_2;
    comlbp_am2s(3)  <= addrmap_o.comlbp_3;
    addrmap_i.comlbp_0  <= comlbp_as2m(0);
    addrmap_i.comlbp_1  <= comlbp_as2m(1);
    addrmap_i.comlbp_2  <= comlbp_as2m(2);
    addrmap_i.comlbp_3  <= comlbp_as2m(3);

    --------------------------------------------


    inst_rx_axis_ic: entity work.axis_com51_rx
    port map (
        aclk                 => pi_payload.m_axi4l_reg_aclk,
        s00_axis_aclk        => pi_payload.m_axi4l_reg_aclk,
        s01_axis_aclk        => pi_payload.m_axi4l_reg_aclk,
        s02_axis_aclk        => pi_payload.m_axi4l_reg_aclk,
        s03_axis_aclk        => pi_payload.m_axi4l_reg_aclk,
        s04_axis_aclk        => pi_payload.m_axi4l_reg_aclk,
        m00_axis_aclk        => pi_payload.m_axi4l_reg_aclk,

        aresetn              => pi_payload.m_axi4l_reg_areset_n,
        s00_axis_aresetn     => pi_payload.m_axi4l_reg_areset_n,
        s01_axis_aresetn     => pi_payload.m_axi4l_reg_areset_n,
        s02_axis_aresetn     => pi_payload.m_axi4l_reg_areset_n,
        s03_axis_aresetn     => pi_payload.m_axi4l_reg_areset_n,
        s04_axis_aresetn     => pi_payload.m_axi4l_reg_areset_n,
        m00_axis_aresetn     => pi_payload.m_axi4l_reg_areset_n,

        s00_axis_tvalid      => axis_combpm_sync_tvalid,
        s00_axis_tready      => axis_combpm_sync_tready,
        s00_axis_tdata       => axis_combpm_sync_tdata,

        s01_axis_tvalid      => axis_comlbp_sync_tvalid(0),
        s01_axis_tready      => axis_comlbp_sync_tready(0),
        s01_axis_tdata       => axis_comlbp_sync_tdata(0),

        s02_axis_tvalid      => axis_comlbp_sync_tvalid(1),
        s02_axis_tready      => axis_comlbp_sync_tready(1),
        s02_axis_tdata       => axis_comlbp_sync_tdata(1),

        s03_axis_tvalid      => axis_comlbp_sync_tvalid(2),
        s03_axis_tready      => axis_comlbp_sync_tready(2),
        s03_axis_tdata       => axis_comlbp_sync_tdata(2),

        s04_axis_tvalid      => axis_comlbp_sync_tvalid(3),
        s04_axis_tready      => axis_comlbp_sync_tready(3),
        s04_axis_tdata       => axis_comlbp_sync_tdata(3),

        m00_axis_tvalid      => axis_pkt_tx_tvalid,
        m00_axis_tready      => axis_pkt_tx_tready,
        m00_axis_tdata       => axis_pkt_tx_tdata,

        s00_arb_req_suppress => c_std_zero,
        s01_arb_req_suppress => c_std_zero,
        s02_arb_req_suppress => c_std_zero,
        s03_arb_req_suppress => c_std_zero,
        s04_arb_req_suppress => c_std_zero
    );


    ------------------
    -- CCN PACKETER --
    ------------------
    inst_ccn_packeter: entity work.top_ccn_packeter
    generic map(
        G_S_TDATA_W          => 80,
        G_S_TUSER_W          => 16,
        G_M_TDATA_W          => 64,
        G_M_TUSER_W          => 1,
        G_PROTOCOL_ID        => 172
    )
    port map(
        aresetn                     => pi_payload.m_axi4l_reg_areset_n,

        -- Running timeref
        timeref                     => timeref,

        -- AXI-MM Status and Config
        aclk                        => pi_payload.m_axi4l_reg_aclk,
        s_axi_m2s                   => addrmap_o.ccn_packeter_0,
        s_axi_s2m                   => addrmap_i.ccn_packeter_0,

        -- AXIS Packet input
        s_axis_tdata                => axis_pkt_tx_tdata(79 downto 0),
        s_axis_tuser                => axis_pkt_tx_tdata(95 downto 80),
        s_axis_tvalid               => axis_pkt_tx_tvalid,
        s_axis_tready               => axis_pkt_tx_tready,

        -- AXIS Frame output
        m_axis_clk                  => clk_comcellnode,
        m_axis_tdata                => axis_tx_tdata,
        m_axis_tkeep                => axis_tx_tkeep,
        m_axis_tlast                => axis_tx_tlast,
        m_axis_tvalid               => axis_tx_tvalid,
        m_axis_tready               => axis_tx_tready,
        m_axis_tuser                => axis_tx_tuser
    );


    -- ============================================================================= --
    -- ==                   COMCELLNODE: GATEWAY TO CENTRALNODE                   == --
    -- ============================================================================= --

    -----------------
    -- COMCELLNODE --
    -----------------
    inst_ccn_ethernet: entity ccn_eth_lib.top_ccn_ethernet
    port map(
        aresetn                     => pi_payload.m_axi4l_reg_areset_n,
        free_100_clk                => pi_payload.ps_clk1,
        aclk                        => pi_payload.m_axi4l_reg_aclk,
        axis_clk                    => clk_comcellnode,

        -- Common Quad signals
        qpll0clk_in                 => gth_qpll0outclk,
        qpll0refclk_in              => gth_qpll0refclk,
        qpll0lock_in                => gth_qpll0lock,
        qpll0reset_out              => gth_qpll0reset,

        -- AXI-MM Status and Config
        s_axi_m2s                   => addrmap_o.ccn_ethernet_0,
        s_axi_s2m                   => addrmap_i.ccn_ethernet_0,

        pps                         => pps,

        -- AXIS TX RX
        s_axis_tx_tvalid            => axis_tx_tvalid,
        s_axis_tx_tready            => axis_tx_tready,
        s_axis_tx_tdata             => axis_tx_tdata,
        s_axis_tx_tkeep             => axis_tx_tkeep,
        s_axis_tx_tlast             => axis_tx_tlast,
        s_axis_tx_tuser             => axis_tx_tuser(0),
        m_axis_rx_tvalid            => axis_rx_tvalid,
        m_axis_rx_tuser             => axis_rx_tuser(0),
        m_axis_rx_tlast             => axis_rx_tlast,
        m_axis_rx_tkeep             => axis_rx_tkeep,
        m_axis_rx_tdata             => axis_rx_tdata,

        -- SFP signals
        sfp_txp                     => fmc2_sfp_tx_p(1),
        sfp_txn                     => fmc2_sfp_tx_n(1),
        sfp_rxp                     => fmc2_sfp_rx_p(1),
        sfp_rxn                     => fmc2_sfp_rx_n(1),
        sfp_rx_los                  => fmc2_sfp_rxlos(1),
        sfp_mod_abs                 => fmc2_sfp_mod_abs(1),
        sfp_tx_disable              => fmc2_sfp_tx_disable(1),
        sfp_tx_fault                => fmc2_sfp_tx_fault(1)
    );

    -- ============================================================================= --
    -- ==      DATA PROCESSING CHAIN: DOWNSTREAM, FROM CENTRALNODE TO PSC         == --
    -- ============================================================================= --
    -- Modules are listed in the signal processing order

    --------------------
    -- CCN UNPACKETER --
    --------------------
    inst_ccn_unpacketer: entity work.top_ccn_unpacketer
    generic map(
        G_S_TDATA_W          => 64,
        G_S_TUSER_W          => 1,
        G_M_TDATA_W          => 48,
        G_M_TUSER_W          => 8,
        G_PROTOCOL_ID        => 182
    )
    port map(
        aresetn                     => pi_payload.m_axi4l_reg_areset_n,

        -- Running timeref
        timeref                     => timeref,

        -- AXI-MM Status and Config
        aclk                        => pi_payload.m_axi4l_reg_aclk,
        s_axi_m2s                   => addrmap_o.ccn_unpacketer_0,
        s_axi_s2m                   => addrmap_i.ccn_unpacketer_0,

        -- AXIS Frame input
        s_axis_clk                  => clk_comcellnode,
        s_axis_tdata                => axis_rx_tdata,
        s_axis_tkeep                => axis_rx_tkeep,
        s_axis_tlast                => axis_rx_tlast,
        s_axis_tvalid               => axis_rx_tvalid,
        s_axis_tready               => axis_rx_tready,
        s_axis_tuser                => axis_rx_tuser,

        -- AXIS Packet output
        m_axis_tdata                => axis_pkt_rx_tdata,
        m_axis_tuser                => axis_pkt_rx_tuser,
        m_axis_tvalid               => axis_pkt_rx_tvalid,
        m_axis_tready               => axis_pkt_rx_tready,
        m_axis_tlast                => axis_pkt_rx_tlast

    );


    axis_pkt_rx_tready <= '1';

    -- TODO: unpacketer detect frame input overrun (tready='1' and tvalid='1')

    ------------
    -- PSCGEN --
    ------------
    inst_pscgen: entity work.top_pscgen
    port map(
        clk             => pi_payload.m_axi4l_reg_aclk,
        rstn            => pi_payload.m_axi4l_reg_areset_n,
        -- AXI MM interface
        s_axi_m2s       => addrmap_o.pscgen_0,
        s_axi_s2m       => addrmap_i.pscgen_0,
        -- AXIS input (not used here, only injection)
        s_axis_tdata_pscid  => (others => '0'),
        s_axis_tdata_value  => (others => '0'),
        s_axis_tuser        => (others => '0'),
        s_axis_tvalid       => '0',
        -- AXIS output
        m_axis_tdata_pscid    => axis_pscgen_tdata_pscid,
        m_axis_tdata_value    => axis_pscgen_tdata_value,
        m_axis_tuser    => axis_pscgen_tuser,
        m_axis_tvalid   => axis_pscgen_tvalid
    );


    --------------------------------
    -- TOGGLE PSCGEN / UNPACKETER --
    --------------------------------
    axis_comcorr_tdata <= x"0000" & axis_pscgen_tdata_value & x"00" & axis_pscgen_tdata_pscid when addrmap_o.output.sel.data(0) = '0' else
                          axis_pkt_rx_tdata(31 downto 0) & axis_pkt_rx_tdata(47 downto 32);
    axis_comcorr_tvalid<= axis_pscgen_tvalid when addrmap_o.output.sel.data(0) = '0' else
                          axis_pkt_rx_tvalid;

    axis_comcorr_tuser <= axis_pscgen_tuser when addrmap_o.output.sel.data(0) = '0' else
                          axis_pkt_rx_tuser;

    -------------
    -- COMCORR --
    -------------
    inst_comcorr:entity work.top_comcorr
    port map(
        clk             => pi_payload.m_axi4l_reg_aclk,
        rstn            => pi_payload.m_axi4l_reg_areset_n,
        -- AXI-MM Status and Config
        s_axi_m2s       => addrmap_o.comcorr_0,
        s_axi_s2m       => addrmap_i.comcorr_0,
        -- AXIS corr com input
        s_axis_tdata    => axis_comcorr_tdata,
        s_axis_tvalid   => axis_comcorr_tvalid,
        -- line outputs
        period_tick     => open,
        line_out        => rtm_tx
    );

    ----------------
    -- RTM CACTUS --
    ----------------
    inst_rtm_cactus: entity work.rtm_cactus_top
    port map(
        c2c_data_p      => pio_payload.c2c_data_p,
        c2c_data_n      => pio_payload.c2c_data_n,
        rtm_tx          => rtm_tx,
        rtm_tx_en       => rtm_tx_en
    );
    rtm_tx_en   <= (others => '1');


    -- ============================================================================= --
    -- ==                           PPS                                           == --
    -- ============================================================================= --
    inst_pps:entity work.pps_cnt
    generic map(
        G_CLK_FREQ  => 100000000,
        G_W_TS      => timeref'length
    )
    port map(
        clk             => pi_payload.m_axi4l_reg_aclk,
        rst_n           => pi_payload.m_axi4l_reg_areset_n,
        srst            => '0',
        pps             => pps,
        timestamp       => timeref
    );
    mc_time <= timeref(mc_time'left downto 0);

    -- ============================================================================= --
    -- ==                          DAQ SYSTEM                                     == --
    -- ============================================================================= --
    inst_daq: entity work.daq_top
    generic map (
        g_fifo_arch                => "ULTRASCALE",
        g_input_data_channel_count => daq_data'length,
        g_axi_data_width           => po_payload.s_axi4_daq.wdata'length,
        g_ext_str_ena              => "11"

    )
    port map (
        pi_clock             => pi_payload.m_axi4l_reg_aclk,
        pi_reset             => rst,
        pi_s_reg_reset       => rst,
        pi_s_reg_if          => addrmap_o.daq_0,
        po_s_reg_if          => addrmap_i.daq_0,
        pi_m_axi4_aclk       => pi_payload.s_axi4_daq_aclk,
        pi_m_axi4_areset_n   => pi_payload.s_axi4_daq_areset_n,
        pi_m_axi4            => m_axi4_daq_s2m,
        po_m_axi4            => m_axi4_daq_m2s,
        -- Triggers and Strobe
        pi_trg(0)            => daq_trigger,
        pi_trg(1)            => daq_trigger,
        pi_ext_str           => daq_strobe,
        pi_transaction_end(0)=> daq_stop,
        pi_transaction_end(1)=> daq_stop,
        -- Data
        pi_data              => daq_data,
        --
        pi_pulse_number      => timeref(31 downto 0)
    );

    m_axi4_daq_s2m <= f_common_to_bsp(pi_payload.s_axi4_daq);
    po_payload.s_axi4_daq <= f_common_to_bsp(m_axi4_daq_m2s);

    -- Mapping data channel
    daq_data(0)                 <= axis_pkt_tx_tdata(31 downto 0);
    daq_data(1)                 <= axis_pkt_tx_tdata(63 downto 32);
    daq_data(2)                 <= axis_pkt_tx_tdata(95 downto 64);
    daq_data(3)(15 downto 0)    <= axis_comcorr_tdata(31 downto 16);
    daq_data(3)(23 downto 16)   <= axis_comcorr_tdata(7 downto 0);
    daq_data(3)(31 downto 24)   <= axis_comcorr_tuser(7 downto 0);
    daq_data(4)                 <= timeref(31 downto  0);

    daq_strobe(0)  <= axis_pkt_tx_tvalid and axis_pkt_tx_tready;
    daq_strobe(1)  <= axis_comcorr_tvalid;

    daq_stop    <= addrmap_o.daq_control.stop.data(0);

    p_daq_trigger:process(pi_payload.m_axi4l_reg_aclk, pi_payload.m_axi4l_reg_areset_n)
    begin
        if pi_payload.m_axi4l_reg_areset_n = '0' then
            daq_trigger     <= '0';
            r_daq_trigger   <= (others => '0');

            ts_strobe_0     <= (others => '0');
            ts_strobe_1     <= (others => '0');
            rs_strobe_0     <= '0';
            rs_strobe_1     <= '0';

        elsif rising_edge(pi_payload.m_axi4l_reg_aclk) then

            r_daq_trigger   <= r_daq_trigger(0) & addrmap_o.daq_control.trigger.data(0);
            daq_trigger     <= (not r_daq_trigger(1)) and r_daq_trigger(0);

            if daq_trigger = '1' then
                -- Trigger set the rs_strobe
                rs_strobe_0 <= '1';
                rs_strobe_1 <= '1';
            else
                -- daq_strobe reset the rs_strobe
                if daq_strobe(0) = '1' then
                    rs_strobe_0 <= '0';
                end if;
                if daq_strobe(1) = '1' then
                    rs_strobe_1 <= '0';
                end if;
            end if;

            -- Refresh the timestamp when rs_strobe is set
            -- The TS freeze on first daq_strobe
            if rs_strobe_0 = '1' then
                ts_strobe_0 <= timeref(31 downto 0);
            end if;

            if rs_strobe_1 = '1' then
                ts_strobe_1 <= timeref(31 downto 0);
            end if;

        end if;
    end process;

    addrmap_i.daq_ts_0.data.data <= ts_strobe_0;
    addrmap_i.daq_ts_1.data.data <= ts_strobe_1;


end architecture struct;
