library ieee;
use ieee.std_logic_1164.all;

Library UNISIM;
use UNISIM.vcomponents.all;

entity gtye_common is
    port(
        -- Differential clock intput
        gtrefclk_p      : in std_logic;
        gtrefclk_n      : in std_logic;

        -- Detection features
        freerun_clk     : in std_logic;

        -- Buffered ref
        buff_gtrefclk   : out std_logic;

        -- QPLL0 interface
        qpll0reset      : in std_logic;
        qpll0lock       : out std_logic;
        qpll0outclk     : out std_logic;
        qpll0refclk     : out std_logic;
        qpll0fblost     : out std_logic;
        qpll0reflost     : out std_logic;

        -- QPLL0 interface
        qpll1reset      : in std_logic;
        qpll1lock       : out std_logic;
        qpll1outclk     : out std_logic;
        qpll1refclk     : out std_logic;
        qpll1fblost     : out std_logic;
        qpll1reflost     : out std_logic

    );
end entity gtye_common;

architecture struct of gtye_common is

    ATTRIBUTE X_INTERFACE_INFO : STRING;
    ATTRIBUTE X_INTERFACE_INFO of gtrefclk_p: SIGNAL is "xilinx.com:interface:diff_clock:1.0 gtrefclk_diff CLK_P";
    ATTRIBUTE X_INTERFACE_INFO of gtrefclk_n: SIGNAL is "xilinx.com:interface:diff_clock:1.0 gtrefclk_diff CLK_N";

    signal gtrefclk : std_logic;
    signal gtrefclk_out2 : std_logic;

    component comlbp_gtwizard_gtye4_common_wrapper
    port(

        GTYE4_COMMON_BGBYPASSB : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_BGMONITORENB : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_BGPDB : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_BGRCALOVRD : in std_logic_vector(4 downto 0);
        GTYE4_COMMON_BGRCALOVRDENB : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_DRPADDR : in std_logic_vector(15 downto 0);
        GTYE4_COMMON_DRPCLK : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_DRPDI : in std_logic_vector(15 downto 0);
        GTYE4_COMMON_DRPEN : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_DRPWE : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_GTGREFCLK0 : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_GTGREFCLK1 : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_GTNORTHREFCLK00 : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_GTNORTHREFCLK01 : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_GTNORTHREFCLK10 : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_GTNORTHREFCLK11 : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_GTREFCLK00 : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_GTREFCLK01 : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_GTREFCLK10 : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_GTREFCLK11 : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_GTSOUTHREFCLK00 : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_GTSOUTHREFCLK01 : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_GTSOUTHREFCLK10 : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_GTSOUTHREFCLK11 : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_PCIERATEQPLL0 : in std_logic_vector(2 downto 0);
        GTYE4_COMMON_PCIERATEQPLL1 : in std_logic_vector(2 downto 0);
        GTYE4_COMMON_PMARSVD0 : in std_logic_vector(7 downto 0);
        GTYE4_COMMON_PMARSVD1 : in std_logic_vector(7 downto 0);
        GTYE4_COMMON_QPLL0CLKRSVD0 : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_QPLL0CLKRSVD1 : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_QPLL0FBDIV : in std_logic_vector(7 downto 0);
        GTYE4_COMMON_QPLL0LOCKDETCLK : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_QPLL0LOCKEN : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_QPLL0PD : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_QPLL0REFCLKSEL : in std_logic_vector(2 downto 0);
        GTYE4_COMMON_QPLL0RESET : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_QPLL1CLKRSVD0 : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_QPLL1CLKRSVD1 : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_QPLL1FBDIV : in std_logic_vector(7 downto 0);
        GTYE4_COMMON_QPLL1LOCKDETCLK : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_QPLL1LOCKEN : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_QPLL1PD : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_QPLL1REFCLKSEL : in std_logic_vector(2 downto 0);
        GTYE4_COMMON_QPLL1RESET : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_QPLLRSVD1 : in std_logic_vector(7 downto 0);
        GTYE4_COMMON_QPLLRSVD2 : in std_logic_vector(4 downto 0);
        GTYE4_COMMON_QPLLRSVD3 : in std_logic_vector(4 downto 0);
        GTYE4_COMMON_QPLLRSVD4 : in std_logic_vector(7 downto 0);
        GTYE4_COMMON_RCALENB : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_SDM0DATA : in std_logic_vector(24 downto 0);
        GTYE4_COMMON_SDM0RESET : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_SDM0TOGGLE : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_SDM0WIDTH : in std_logic_vector(1 downto 0);
        GTYE4_COMMON_SDM1DATA : in std_logic_vector(24 downto 0);
        GTYE4_COMMON_SDM1RESET : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_SDM1TOGGLE : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_SDM1WIDTH : in std_logic_vector(1 downto 0);
        GTYE4_COMMON_UBCFGSTREAMEN : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_UBDO : in std_logic_vector(15 downto 0);
        GTYE4_COMMON_UBDRDY : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_UBENABLE : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_UBGPI : in std_logic_vector(1 downto 0);
        GTYE4_COMMON_UBINTR : in std_logic_vector(1 downto 0);
        GTYE4_COMMON_UBIOLMBRST : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_UBMBRST : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_UBMDMCAPTURE : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_UBMDMDBGRST : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_UBMDMDBGUPDATE : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_UBMDMREGEN : in std_logic_vector(3 downto 0);
        GTYE4_COMMON_UBMDMSHIFT : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_UBMDMSYSRST : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_UBMDMTCK : in std_logic_vector(0 downto 0);
        GTYE4_COMMON_UBMDMTDI : in std_logic_vector(0 downto 0);

        GTYE4_COMMON_DRPDO : out std_logic_vector(15 downto 0);
        GTYE4_COMMON_DRPRDY : out std_logic_vector(0 downto 0);
        GTYE4_COMMON_PMARSVDOUT0 : out std_logic_vector(7 downto 0);
        GTYE4_COMMON_PMARSVDOUT1 : out std_logic_vector(7 downto 0);
        GTYE4_COMMON_QPLL0FBCLKLOST : out std_logic_vector(0 downto 0);
        GTYE4_COMMON_QPLL0LOCK : out std_logic_vector(0 downto 0);
        GTYE4_COMMON_QPLL0OUTCLK : out std_logic_vector(0 downto 0);
        GTYE4_COMMON_QPLL0OUTREFCLK : out std_logic_vector(0 downto 0);
        GTYE4_COMMON_QPLL0REFCLKLOST : out std_logic_vector(0 downto 0);
        GTYE4_COMMON_QPLL1FBCLKLOST : out std_logic_vector(0 downto 0);
        GTYE4_COMMON_QPLL1LOCK : out std_logic_vector(0 downto 0);
        GTYE4_COMMON_QPLL1OUTCLK : out std_logic_vector(0 downto 0);
        GTYE4_COMMON_QPLL1OUTREFCLK : out std_logic_vector(0 downto 0);
        GTYE4_COMMON_QPLL1REFCLKLOST : out std_logic_vector(0 downto 0);
        GTYE4_COMMON_QPLLDMONITOR0 : out std_logic_vector(7 downto 0);
        GTYE4_COMMON_QPLLDMONITOR1 : out std_logic_vector(7 downto 0);
        GTYE4_COMMON_REFCLKOUTMONITOR0 : out std_logic_vector(0 downto 0);
        GTYE4_COMMON_REFCLKOUTMONITOR1 : out std_logic_vector(0 downto 0);
        GTYE4_COMMON_RXRECCLK0SEL : out std_logic_vector(1 downto 0);
        GTYE4_COMMON_RXRECCLK1SEL : out std_logic_vector(1 downto 0);
        GTYE4_COMMON_SDM0FINALOUT : out std_logic_vector(3 downto 0);
        GTYE4_COMMON_SDM0TESTDATA : out std_logic_vector(14 downto 0);
        GTYE4_COMMON_SDM1FINALOUT : out std_logic_vector(3 downto 0);
        GTYE4_COMMON_SDM1TESTDATA : out std_logic_vector(14 downto 0);
        GTYE4_COMMON_UBDADDR : out std_logic_vector(15 downto 0);
        GTYE4_COMMON_UBDEN : out std_logic_vector(0 downto 0);
        GTYE4_COMMON_UBDI : out std_logic_vector(15 downto 0);
        GTYE4_COMMON_UBDWE : out std_logic_vector(0 downto 0);
        GTYE4_COMMON_UBMDMTDO : out std_logic_vector(0 downto 0);
        GTYE4_COMMON_UBRSVDOUT : out std_logic_vector(0 downto 0);
        GTYE4_COMMON_UBTXUART : out std_logic_vector(0 downto 0)

        );
    end component;

begin

    -- INPUT BUFFER
   IBUFDS_GTE4_inst : IBUFDS_GTE4
   generic map (
      REFCLK_EN_TX_PATH => '0',
      REFCLK_HROW_CK_SEL => "00",
      REFCLK_ICNTL_RX => "00"
   )
   port map (
      O => gtrefclk,
      ODIV2 => gtrefclk_out2,
      CEB => '0',
      I => gtrefclk_p,
      IB => gtrefclk_n
   );

   BUFG_GT_inst : BUFG_GT
   port map (
      O => buff_gtrefclk,
      CE => '1',
      CEMASK => '1',
      CLR => '0',
      CLRMASK => '1',
      DIV => "000",
      I => gtrefclk_out2
   );

    -- TRANSCEIVER COMMON BLOCK
   gtye_common_wrapper_inst: comlbp_gtwizard_gtye4_common_wrapper
   port map(
        GTYE4_COMMON_BGBYPASSB         => "1",
        GTYE4_COMMON_BGMONITORENB      => "1",
        GTYE4_COMMON_BGPDB             => "1",
        GTYE4_COMMON_BGRCALOVRD        => "10000",
        GTYE4_COMMON_BGRCALOVRDENB     => "1",
        GTYE4_COMMON_DRPADDR           => "0000000000000000",
        GTYE4_COMMON_DRPCLK            => "0",
        GTYE4_COMMON_DRPDI             => "0000000000000000",
        GTYE4_COMMON_DRPEN             => "0",
        GTYE4_COMMON_DRPWE             => "0",
        GTYE4_COMMON_GTGREFCLK0        => "0",
        GTYE4_COMMON_GTGREFCLK1        => "0",
        GTYE4_COMMON_GTNORTHREFCLK00   => "0",
        GTYE4_COMMON_GTNORTHREFCLK01   => "0",
        GTYE4_COMMON_GTNORTHREFCLK10   => "0",
        GTYE4_COMMON_GTNORTHREFCLK11   => "0",
        GTYE4_COMMON_GTREFCLK00(0)     => gtrefclk,
        GTYE4_COMMON_GTREFCLK01(0)     => gtrefclk,
        GTYE4_COMMON_GTREFCLK10        => "0",
        GTYE4_COMMON_GTREFCLK11        => "0",
        GTYE4_COMMON_GTSOUTHREFCLK00   => "0",
        GTYE4_COMMON_GTSOUTHREFCLK01   => "0",
        GTYE4_COMMON_GTSOUTHREFCLK10   => "0",
        GTYE4_COMMON_GTSOUTHREFCLK11   => "0",
        GTYE4_COMMON_PCIERATEQPLL0     => "000",
        GTYE4_COMMON_PCIERATEQPLL1     => "000",
        GTYE4_COMMON_PMARSVD0          => "00000000",
        GTYE4_COMMON_PMARSVD1          => "00000000",
        GTYE4_COMMON_QPLL0CLKRSVD0     => "0",
        GTYE4_COMMON_QPLL0CLKRSVD1     => "0",
        GTYE4_COMMON_QPLL0FBDIV        => "00000000",
        GTYE4_COMMON_QPLL0LOCKDETCLK(0)=> freerun_clk,
        GTYE4_COMMON_QPLL0LOCKEN       => "1",
        GTYE4_COMMON_QPLL0PD           => "0",
        GTYE4_COMMON_QPLL0REFCLKSEL    => "001",
        GTYE4_COMMON_QPLL0RESET(0)     => qpll0reset,
        GTYE4_COMMON_QPLL1CLKRSVD0     => "0",
        GTYE4_COMMON_QPLL1CLKRSVD1     => "0",
        GTYE4_COMMON_QPLL1FBDIV        => "00000000",
        GTYE4_COMMON_QPLL1LOCKDETCLK(0)=> freerun_clk,
        GTYE4_COMMON_QPLL1LOCKEN       => "1",
        GTYE4_COMMON_QPLL1PD           => "0",
        GTYE4_COMMON_QPLL1REFCLKSEL    => "001",
        GTYE4_COMMON_QPLL1RESET(0)     => qpll1reset,
        GTYE4_COMMON_QPLLRSVD1         => "00000000",
        GTYE4_COMMON_QPLLRSVD2         => "00000",
        GTYE4_COMMON_QPLLRSVD3         => "00000",
        GTYE4_COMMON_QPLLRSVD4         => "00000000",
        GTYE4_COMMON_RCALENB           => "1",
        GTYE4_COMMON_SDM0DATA          => "0000000000000000000000000",
        GTYE4_COMMON_SDM0RESET         => "0",
        GTYE4_COMMON_SDM0TOGGLE        => "0",
        GTYE4_COMMON_SDM0WIDTH         => "00",
        GTYE4_COMMON_SDM1DATA          => "0001100110011001100110011",
        GTYE4_COMMON_SDM1RESET         => "0",
        GTYE4_COMMON_SDM1TOGGLE        => "0",
        GTYE4_COMMON_SDM1WIDTH         => "00",
        GTYE4_COMMON_UBCFGSTREAMEN     => "0",
        GTYE4_COMMON_UBDO              => "0000000000000000",
        GTYE4_COMMON_UBDRDY            => "0",
        GTYE4_COMMON_UBENABLE          => "0",
        GTYE4_COMMON_UBGPI             => "00",
        GTYE4_COMMON_UBINTR            => "00",
        GTYE4_COMMON_UBIOLMBRST        => "0",
        GTYE4_COMMON_UBMBRST           => "0",
        GTYE4_COMMON_UBMDMCAPTURE      => "0",
        GTYE4_COMMON_UBMDMDBGRST       => "0",
        GTYE4_COMMON_UBMDMDBGUPDATE    => "0",
        GTYE4_COMMON_UBMDMREGEN        => "0000",
        GTYE4_COMMON_UBMDMSHIFT        => "0",
        GTYE4_COMMON_UBMDMSYSRST       => "0",
        GTYE4_COMMON_UBMDMTCK          => "0",
        GTYE4_COMMON_UBMDMTDI          => "0",
        GTYE4_COMMON_DRPDO             => open,
        GTYE4_COMMON_DRPRDY            => open,
        GTYE4_COMMON_PMARSVDOUT0       => open,
        GTYE4_COMMON_PMARSVDOUT1       => open,
        GTYE4_COMMON_QPLL0FBCLKLOST(0) => qpll0fblost,
        GTYE4_COMMON_QPLL0LOCK(0)      => qpll0lock,
        GTYE4_COMMON_QPLL0OUTCLK(0)    => qpll0outclk,
        GTYE4_COMMON_QPLL0OUTREFCLK(0) => qpll0refclk,
        GTYE4_COMMON_QPLL0REFCLKLOST(0)=> qpll0reflost,
        GTYE4_COMMON_QPLL1FBCLKLOST(0) => qpll1fblost,
        GTYE4_COMMON_QPLL1LOCK(0)      => qpll1lock,
        GTYE4_COMMON_QPLL1OUTCLK(0)    => qpll1outclk,
        GTYE4_COMMON_QPLL1OUTREFCLK(0) => qpll1refclk,
        GTYE4_COMMON_QPLL1REFCLKLOST(0)=> qpll1reflost,
        GTYE4_COMMON_QPLLDMONITOR0     => open,
        GTYE4_COMMON_QPLLDMONITOR1     => open,
        GTYE4_COMMON_REFCLKOUTMONITOR0 => open,
        GTYE4_COMMON_REFCLKOUTMONITOR1 => open,
        GTYE4_COMMON_RXRECCLK0SEL      => open,
        GTYE4_COMMON_RXRECCLK1SEL      => open,
        GTYE4_COMMON_SDM0FINALOUT      => open,
        GTYE4_COMMON_SDM0TESTDATA      => open,
        GTYE4_COMMON_SDM1FINALOUT      => open,
        GTYE4_COMMON_SDM1TESTDATA      => open,
        GTYE4_COMMON_UBDADDR           => open,
        GTYE4_COMMON_UBDEN             => open,
        GTYE4_COMMON_UBDI              => open,
        GTYE4_COMMON_UBDWE             => open,
        GTYE4_COMMON_UBMDMTDO          => open,
        GTYE4_COMMON_UBRSVDOUT         => open,
        GTYE4_COMMON_UBTXUART          => open
    );

end architecture struct;
