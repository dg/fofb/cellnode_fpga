

set ProjectDirPath [file join ${::fwfwk::PrjBuildPath} ${::fwfwk::PrjBuildName}]

## ------------------------------------- ##
## UPSTREAM INTERCONNECT
## ------------------------------------- ##
set ipName "axis_com51_rx"
# 5 to 1 axis interconnect for bpm/lpb reception

set xcipath [create_ip \
    -name axis_interconnect \
    -vendor xilinx.com \
    -library ip -version 1.1 \
    -module_name $ipName]

set_property -dict [list \
    CONFIG.C_NUM_SI_SLOTS {5} \
    CONFIG.SWITCH_TDATA_NUM_BYTES {12} \
    CONFIG.HAS_TSTRB {false} \
    CONFIG.HAS_TKEEP {false} \
    CONFIG.HAS_TLAST {false} \
    CONFIG.HAS_TID {false} \
    CONFIG.HAS_TDEST {false} \
    CONFIG.HAS_TUSER {false} \
    CONFIG.SWITCH_TUSER_BITS_PER_BYTE {1} \
    CONFIG.ARBITER_TYPE {Fixed} \
    CONFIG.C_S00_AXIS_IS_ACLK_ASYNC {0} \
    CONFIG.C_S01_AXIS_IS_ACLK_ASYNC {1} \
    CONFIG.C_S02_AXIS_IS_ACLK_ASYNC {1} \
    CONFIG.C_S03_AXIS_IS_ACLK_ASYNC {1} \
    CONFIG.C_S04_AXIS_IS_ACLK_ASYNC {1} \
    CONFIG.M00_AXIS_FIFO_MODE {1_(Normal)} \
    CONFIG.C_M00_AXIS_FIFO_DEPTH {128} \
    CONFIG.S00_AXIS_FIFO_MODE {1_(Normal)} \
    CONFIG.C_S00_AXIS_FIFO_DEPTH {128} \
    CONFIG.S01_AXIS_FIFO_MODE {1_(Normal)} \
    CONFIG.C_S01_AXIS_FIFO_DEPTH {128} \
    CONFIG.S02_AXIS_FIFO_MODE {1_(Normal)} \
    CONFIG.C_S02_AXIS_FIFO_DEPTH {128} \
    CONFIG.S03_AXIS_FIFO_MODE {1_(Normal)} \
    CONFIG.C_S03_AXIS_FIFO_DEPTH {128} \
    CONFIG.S04_AXIS_FIFO_MODE {1_(Normal)} \
    CONFIG.C_S04_AXIS_FIFO_DEPTH {128} \
    CONFIG.SWITCH_PACKET_MODE {false} \
    CONFIG.C_SWITCH_MAX_XFERS_PER_ARB {1} \
    CONFIG.C_SWITCH_NUM_CYCLES_TIMEOUT {0} \
    CONFIG.M00_AXIS_TDATA_NUM_BYTES {12} \
    CONFIG.S00_AXIS_TDATA_NUM_BYTES {12} \
    CONFIG.S01_AXIS_TDATA_NUM_BYTES {12} \
    CONFIG.S02_AXIS_TDATA_NUM_BYTES {12} \
    CONFIG.S03_AXIS_TDATA_NUM_BYTES {12} \
    CONFIG.S04_AXIS_TDATA_NUM_BYTES {12} \
    CONFIG.M00_S00_CONNECTIVITY {true} \
    CONFIG.M00_S01_CONNECTIVITY {true} \
    CONFIG.M00_S02_CONNECTIVITY {true} \
    CONFIG.M00_S03_CONNECTIVITY {true} \
    CONFIG.M00_S04_CONNECTIVITY {true} \
] [get_ips $ipName]

generate_target all [get_files  ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci]
export_ip_user_files -of_objects [get_files ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1] ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci]



