################################################################################
# Main tcl for the module
################################################################################

# ==============================================================================
proc init {} {

    # Common modules
    addSrcModule ccn_ethernet $::fwfwk::SrcPath/comcellnode_ethernet/tcl/main.tcl
    addSrcModule ccn_packet $::fwfwk::SrcPath/ccn_packet/tcl/main.tcl
    addSrcModule fmc_4sfp $::fwfwk::SrcPath/fmc_4sfp/tcl/main.tcl
    addSrcModule daq $::fwfwk::SrcPath/daq/tcl/main.tcl
    addSrcModule pscgen $::fwfwk::SrcPath/pscgen/tcl/main.tcl

    # Modules for CellNodes
    if {$::fwfwk::ProjectConf == "CellNode"} {
        addSrcModule combpm $::fwfwk::SrcPath/combpm/tcl/main.tcl
        addSrcModule comlbp $::fwfwk::SrcPath/comlbp/tcl/main.tcl
        addSrcModule comcorr $::fwfwk::SrcPath/comcorr/tcl/main.tcl
    }

    # Modules for CellNodes
    if {$::fwfwk::ProjectConf == "CentralNode"} {
        addSrcModule corr_matrix $::fwfwk::SrcPath/corr_matrix/tcl/main.tcl
    }

    # Common configuration
    set ccn_packet::CCN_FRAME_W 64
    set ccn_packet::CCN_FRAME_HEADER_W 192
    set ccn_packet::CCN_UPKT_TU_W 8

    set daq::Config(C_DAQ_REGIONS) 2

    if {$::fwfwk::ProjectConf == "CellNode"} {
        set ccn_packet::CCN_DPKT_TU_W 16
        set ccn_packet::CCN_DPKT_W 80
        set ccn_packet::CCN_UPKT_W 48
        set comlbp::Config(GT_LOC) [list X0Y4 X0Y5 X0Y6 X0Y7]
        set ccn_ethernet::Config(GTH_LOC) [list X0Y5]
    }
    if {$::fwfwk::ProjectConf == "CentralNode"} {
        set ccn_packet::CCN_DPKT_TU_W 8
        set ccn_packet::CCN_DPKT_W 48
        set ccn_packet::CCN_UPKT_W 80
        set ccn_ethernet::Config(GTH_LOC) [list X0Y4 X0Y5 X0Y6 X0Y7]
    }

}

# ==============================================================================
proc setSources {} {
    variable Vhdl
    variable Verilog
    variable Xdc

    genModVerFile VHDL ../hdl/pkg_app_fofb_version.vhd
    lappend Vhdl ../hdl/pkg_app_fofb_version.vhd
    lappend Vhdl ../hdl/pps_cnt.vhd

    # ----------------------------------
    # Configuration related sources
    if {$::fwfwk::ProjectConf == "CellNode"} {
        lappend Vhdl {"../hdl/top_app_cellnode.vhd" "VHDL 2008"}
        lappend Vhdl ../hdl/pkg_app_fofb_cellnode.vhd
        lappend Vhdl ../hdl/rtm_cactus_top.vhd
        lappend Vhdl ../hdl/gtye_common.vhd
        lappend Verilog ../hdl/comlbp_gtwizard_gtye4_common_wrapper.v
        lappend Xdc ../con/physical_c2c.xdc
        lappend Xdc ../con/physical_fmc1.xdc
    }

    if {$::fwfwk::ProjectConf == "CentralNode"} {
        lappend Vhdl ../hdl/top_app_centralnode.vhd
        lappend Vhdl ../hdl/pkg_app_fofb_centralnode.vhd
    }
    # ----------------------------------

    # Sources for QPLL common
    lappend Vhdl ../hdl/gthe_common.vhd
    lappend Verilog ../hdl/combpm_gtwizard_gthe4_common_wrapper.v

    lappend Xdc ../con/physical_fmc2.xdc
}

# ==============================================================================
proc setAddressSpace {} {
    variable AddressSpace
    variable AddressSpaceDAQ

    # Root space
    if {$::fwfwk::ProjectConf == "CellNode"} {
        addAddressSpace AddressSpace "app_fofb_cellnode"            RDL     {}          ../rdl/app_fofb_cellnode.rdl
    }
    if {$::fwfwk::ProjectConf == "CentralNode"} {
        addAddressSpace AddressSpace "app_fofb_centralnode"         RDL     {}          ../rdl/app_fofb_centralnode.rdl
    }

    # Elements
    addAddressSpace AddressSpace "ccn_ethernet_0"           INST    {}      ccn_ethernet::AddressSpace
    addAddressSpace AddressSpace "ccn_packeter_0"           INST    {}      ccn_packet::AddressSpacePack
    addAddressSpace AddressSpace "ccn_unpacketer_0"         INST    {}      ccn_packet::AddressSpaceUnpack
    addAddressSpace AddressSpace "pscgen_0"                 INST    {}      pscgen::AddressSpace
    addAddressSpace AddressSpace "daq_0"                    INST    {}      daq::AddressSpace

    if {$::fwfwk::ProjectConf == "CellNode"} {
        addAddressSpace AddressSpace "combpm_0"                 INST    {}      combpm::AddressSpace
        addAddressSpace AddressSpace "comlbp_0"                 INST    {}      comlbp::AddressSpace
        addAddressSpace AddressSpace "comcorr_0"                INST    {}      comcorr::AddressSpace
    }

    if {$::fwfwk::ProjectConf == "CentralNode"} {
        addAddressSpace AddressSpace "ccn_ethernet_1"           INST    {}      ccn_ethernet::AddressSpace
        addAddressSpace AddressSpace "ccn_ethernet_2"           INST    {}      ccn_ethernet::AddressSpace
        addAddressSpace AddressSpace "ccn_ethernet_3"           INST    {}      ccn_ethernet::AddressSpace
        addAddressSpace AddressSpace "ccn_packeter_1"           INST    {}      ccn_packet::AddressSpacePack
        addAddressSpace AddressSpace "ccn_unpacketer_1"         INST    {}      ccn_packet::AddressSpaceUnpack
        addAddressSpace AddressSpace "ccn_packeter_2"           INST    {}      ccn_packet::AddressSpacePack
        addAddressSpace AddressSpace "ccn_unpacketer_2"         INST    {}      ccn_packet::AddressSpaceUnpack
        addAddressSpace AddressSpace "ccn_packeter_3"           INST    {}      ccn_packet::AddressSpacePack
        addAddressSpace AddressSpace "ccn_unpacketer_3"         INST    {}      ccn_packet::AddressSpaceUnpack
        addAddressSpace AddressSpace "corr_matrix_0"            INST    {}      corr_matrix::AddressSpace
    }

    addAddressSpace AddressSpaceDAQ  "app_daq"                  RDL     {}      ../rdl/app_daq.rdl
}

# ==============================================================================
proc doOnCreate {} {
    variable Vhdl
    variable Verilog
    variable Xdc

    addSources Vhdl
    addSources Verilog
    addSources Xdc

    # Generate Xilinx IP
    if {$::fwfwk::ProjectConf == "CellNode"} {
        source ../tcl/gen_ip_cellnode.tcl
    }
    if {$::fwfwk::ProjectConf == "CentralNode"} {
        source ../tcl/gen_ip_centralnode.tcl
    }

}

# ==============================================================================
proc doOnBuild {} {
}

# ==============================================================================
proc setSim {} {
}

