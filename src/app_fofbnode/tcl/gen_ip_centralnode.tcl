

set ProjectDirPath [file join ${::fwfwk::PrjBuildPath} ${::fwfwk::PrjBuildName}]

## ------------------------------------- ##
## UPSTREAM INTERCONNECT
## ------------------------------------- ##
set ipName "axis_ic41_rx"

set xcipath [create_ip \
    -name axis_interconnect \
    -vendor xilinx.com \
    -library ip -version 1.1 \
    -module_name $ipName]

set_property -dict [list \
    CONFIG.C_NUM_SI_SLOTS {4} \
    CONFIG.SWITCH_TDATA_NUM_BYTES {10} \
    CONFIG.HAS_TSTRB {false} \
    CONFIG.HAS_TKEEP {false} \
    CONFIG.HAS_TLAST {false} \
    CONFIG.HAS_TID {true} \
    CONFIG.HAS_TDEST {false} \
    CONFIG.HAS_TUSER {true} \
    CONFIG.SWITCH_TUSER_BITS_PER_BYTE {1} \
    CONFIG.C_SWITCH_TID_WIDTH {2} \
    CONFIG.ARBITER_TYPE {Fixed} \
    CONFIG.S00_AXIS_FIFO_MODE {1_(Normal)} \
    CONFIG.C_S00_AXIS_FIFO_DEPTH {128} \
    CONFIG.S01_AXIS_FIFO_MODE {1_(Normal)} \
    CONFIG.C_S01_AXIS_FIFO_DEPTH {128} \
    CONFIG.S02_AXIS_FIFO_MODE {1_(Normal)} \
    CONFIG.C_S02_AXIS_FIFO_DEPTH {128} \
    CONFIG.S03_AXIS_FIFO_MODE {1_(Normal)} \
    CONFIG.C_S03_AXIS_FIFO_DEPTH {128} \
    CONFIG.SWITCH_PACKET_MODE {false} \
    CONFIG.C_SWITCH_MAX_XFERS_PER_ARB {1} \
    CONFIG.C_SWITCH_NUM_CYCLES_TIMEOUT {0} \
    CONFIG.M00_AXIS_TDATA_NUM_BYTES {10} \
    CONFIG.S00_AXIS_TDATA_NUM_BYTES {10} \
    CONFIG.S01_AXIS_TDATA_NUM_BYTES {10} \
    CONFIG.S02_AXIS_TDATA_NUM_BYTES {10} \
    CONFIG.S03_AXIS_TDATA_NUM_BYTES {10} \
    CONFIG.M00_S01_CONNECTIVITY {true} \
    CONFIG.M00_S02_CONNECTIVITY {true} \
    CONFIG.M00_S03_CONNECTIVITY {true} \
] [get_ips $ipName]

generate_target all [get_files  ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci]
export_ip_user_files -of_objects [get_files ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1] ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci]



## ------------------------------------- ##
## DOWNSTREAM BROADCASTER
## ------------------------------------- ##
set ipName "axis_bc14_tx"

set xcipath [create_ip \
    -name axis_broadcaster \
    -vendor xilinx.com \
    -library ip -version 1.1 \
    -module_name $ipName]

set_property -dict [list \
    CONFIG.NUM_MI {4} \
    CONFIG.M_TDATA_NUM_BYTES {6} \
    CONFIG.S_TDATA_NUM_BYTES {6} \
    CONFIG.M_TUSER_WIDTH {8} \
    CONFIG.S_TUSER_WIDTH {8} \
    CONFIG.M00_TDATA_REMAP {tdata[47:0]} \
    CONFIG.M01_TDATA_REMAP {tdata[47:0]} \
    CONFIG.M02_TDATA_REMAP {tdata[47:0]} \
    CONFIG.M03_TDATA_REMAP {tdata[47:0]} \
    CONFIG.M00_TUSER_REMAP {tuser[7:0]} \
    CONFIG.M01_TUSER_REMAP {tuser[7:0]} \
    CONFIG.M02_TUSER_REMAP {tuser[7:0]} \
    CONFIG.M03_TUSER_REMAP {tuser[7:0]} \
] [get_ips $ipName]

generate_target all [get_files  ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci]
export_ip_user_files -of_objects [get_files ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1] ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci]


## ------------------------------------- ##
## DOWNSTREAM FIFO
## ------------------------------------- ##
set ipName "axis_downstream_fifo"

set xcipath [create_ip \
    -name axis_data_fifo \
    -vendor xilinx.com \
    -library ip -version 2.0 \
    -module_name $ipName]

set_property -dict [list \
    CONFIG.TDATA_NUM_BYTES {3} \
    CONFIG.TUSER_WIDTH {8} \
    CONFIG.FIFO_DEPTH {64} \
    CONFIG.IS_ACLK_ASYNC {0} \
    CONFIG.HAS_AFULL {1} \
] [get_ips $ipName]

generate_target all [get_files  ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci]
export_ip_user_files -of_objects [get_files ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1] ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci]

## ------------------------------------- ##
## CAPTURE INTERCONNECT
## ------------------------------------- ##
set ipName "axis_ic_capt"

set xcipath [create_ip \
    -name axis_interconnect \
    -vendor xilinx.com \
    -library ip -version 1.1 \
    -module_name $ipName]

set_property -dict [list \
    CONFIG.C_NUM_SI_SLOTS {4} \
    CONFIG.SWITCH_TDATA_NUM_BYTES {8} \
    CONFIG.HAS_TSTRB {false} \
    CONFIG.HAS_TKEEP {false} \
    CONFIG.HAS_TLAST {false} \
    CONFIG.HAS_TID {false} \
    CONFIG.HAS_TDEST {false} \
    CONFIG.HAS_TUSER {false} \
    CONFIG.ARBITER_TYPE {Fixed} \
    CONFIG.S00_AXIS_FIFO_MODE {1_(Normal)} \
    CONFIG.C_S00_AXIS_FIFO_DEPTH {128} \
    CONFIG.S01_AXIS_FIFO_MODE {1_(Normal)} \
    CONFIG.C_S01_AXIS_FIFO_DEPTH {128} \
    CONFIG.S02_AXIS_FIFO_MODE {1_(Normal)} \
    CONFIG.C_S02_AXIS_FIFO_DEPTH {128} \
    CONFIG.S03_AXIS_FIFO_MODE {1_(Normal)} \
    CONFIG.C_S03_AXIS_FIFO_DEPTH {128} \
    CONFIG.SWITCH_PACKET_MODE {false} \
    CONFIG.C_SWITCH_MAX_XFERS_PER_ARB {1} \
    CONFIG.C_SWITCH_NUM_CYCLES_TIMEOUT {0} \
    CONFIG.M00_AXIS_TDATA_NUM_BYTES {8} \
    CONFIG.S00_AXIS_TDATA_NUM_BYTES {8} \
    CONFIG.S01_AXIS_TDATA_NUM_BYTES {8} \
    CONFIG.S02_AXIS_TDATA_NUM_BYTES {8} \
    CONFIG.S03_AXIS_TDATA_NUM_BYTES {8} \
    CONFIG.M00_S01_CONNECTIVITY {true} \
    CONFIG.M00_S02_CONNECTIVITY {true} \
    CONFIG.M00_S03_CONNECTIVITY {true} \
] [get_ips $ipName]

generate_target all [get_files  ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci]
export_ip_user_files -of_objects [get_files ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1] ${ProjectDirPath}.srcs/sources_1/ip/$ipName/$ipName.xci]



